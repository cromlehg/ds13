package ds13.bots.storable.insta

import ds13.bots.storable.Proxy
import ds13.common.Logger
import ds13.http.TraitProxyProperties

class ConvertedProxy(override val logger: Logger, val proxy: Proxy) extends TraitProxyProperties {

  def getIP: String = proxy.ip

  def getPort: Option[Int] = proxy.port

  def getLogin: Option[String] = proxy.login

  def getPass: Option[String] = proxy.pass

}