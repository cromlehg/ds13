package ds13.bots.storable.insta

import org.json.JSONException
import org.json.JSONObject

import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.StorableBot
import ds13.bots.storable.StorableBotProcessStates
import ds13.insta.api.InstaAPIExecutor
import ds13.insta.api.CheckpointLoggedOut

class InstaBot(
  config: TraitBotConfig,
  logger: TraitBotProcessLogger)
    extends StorableBot(config, logger) {

  var api: InstaAPIExecutor = null

  override def catchedStep: Unit =
    state match {
      case StorableBotProcessStates.STATE_STORABLE_INIT_FINISHED =>
        setState(InstaBotProcessStates.STATE_INSTA_BOT_INIT)
      case InstaBotProcessStates.STATE_INSTA_BOT_INIT =>
        debug("Initialize and convert proxy...")
        val convertedProxy = launchStorage.accountStorage.getProxy map (t => new ConvertedProxy(logger, t))
        debug("Proxy successfully converted!")
        debug("Reading account properties...")
        launchStorage.accountStorage.account.properties
          .fold(errFinish("Properties for account wiht id " + launchStorage.accountStorage.account.id + " is empty!")) { props =>
            try {
              // Should convert to Ether like  this:
              /*
  def f1(v: String): Option[String] = {
    println("called f1")
    Some(v)
  }

  def f2(v: String): Option[String] = {
    println("called f2")
    Some(v)
  }

  val x = "x"

  val y = "y"  val either = for {
    y <- f1(x).toRight(println("error1")).right
    z <- f2(y).toRight(println("error2")).right
  } yield z

  either.merge
               */
              val jsonObject = new JSONObject(props)
              jsonAccountProps(jsonObject, "deviceId") { deviceId: String =>
                jsonAccountProps(jsonObject, "GUID") { GUID: String =>
                  jsonAccountProps(jsonObject, "username") { username: String =>
                    jsonAccountProps(jsonObject, "password") { password: String =>
                      api = new InstaAPIExecutor(
                        logger,
                        convertedProxy,
                        "5.0.7",
                        "Android (19/4.4.2; 160dpi; 720x1104; Samsung; GT-I9500; ja3g; unknown; en_EN)",
                        deviceId,
                        GUID,
                        username,
                        password)
                      debug("Insta bot initialization successfully fininshed for account " + launchStorage.accountStorage.account.id)
                      setState(InstaBotProcessStates.STATE_INSTA_BOT_INIT_FINISHED)
                    }
                  }
                }
              }
            } catch {
              case e: JSONException =>
                errFinish("Exception occurred during parse JSON properties for account "
                  + launchStorage.accountStorage.account.id +
                  " from: " + props + ", " + e.getMessage)
            }
          }
      case unknwonState =>
        super.catchedStep
    }

  protected def jsonAccountProps[A, B](jsonObject: JSONObject, fieldName: String)(success: A => B) =
    if ((jsonObject.has(fieldName) && !jsonObject.isNull(fieldName)))
      success(jsonObject.get(fieldName).asInstanceOf[A])
    else
      errFinish("Exception occurred during parse JSON properties for account "
        + launchStorage.accountStorage.account.id +
        " from: " + jsonObject.toString() + ", " + fieldName + " - not found!")

}

