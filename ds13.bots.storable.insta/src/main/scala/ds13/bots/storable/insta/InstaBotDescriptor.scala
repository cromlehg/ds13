package ds13.bots.storable.insta

import ds13.bots.BotServiceFactory
import ds13.bots.BotSystem
import ds13.bots.storable.StorableBotDescriptor

abstract class InstaBotDescriptor(
  system: BotSystem,
  id: Long,
  serviceFactory: BotServiceFactory,
  accountTypeId: Long)
    extends StorableBotDescriptor(
      system,
      id,
      serviceFactory,
      accountTypeId) {

  override def createBotLaunchHelper: InstaBotLaunchHelper

}