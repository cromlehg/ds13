package ds13.bots.storable.insta

import ds13.bots.BotServiceFactory
import ds13.bots.BotSystem
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.StorableBotLaunchHelper
import ds13.common.Logger

abstract class InstaBotLaunchHelper(
  system: BotSystem,
  logger: Logger,
  serviceFactory: BotServiceFactory,
  descriptorId: Long,
  accountTypeId: Long)
    extends StorableBotLaunchHelper(
      system,
      logger,
      serviceFactory,
      descriptorId,
      accountTypeId) {

  override protected def createBotProcess(config: TraitBotConfig, logger: TraitBotProcessLogger): InstaBot

}