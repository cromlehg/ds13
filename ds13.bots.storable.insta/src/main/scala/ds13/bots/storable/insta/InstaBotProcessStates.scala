package ds13.bots.storable.insta

import ds13.bots.storable.StorableBotProcessStates

object InstaBotProcessStates {

  val STATE_INSTA_BOT_INIT = StorableBotProcessStates.FIRST_FREE_STATE_INDEX

  val STATE_INSTA_BOT_INIT_FINISHED = STATE_INSTA_BOT_INIT + 1

  val FIRST_FREE_STATE_INDEX = STATE_INSTA_BOT_INIT_FINISHED + 1

}