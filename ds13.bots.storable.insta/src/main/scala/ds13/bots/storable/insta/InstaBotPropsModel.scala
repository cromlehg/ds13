package ds13.bots.storable.insta

class InstaBotPropsModel(
  val deviceId: String,
  val GUID: String,
  val username: String,
  val password: String)