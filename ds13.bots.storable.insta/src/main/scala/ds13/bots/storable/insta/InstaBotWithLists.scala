package ds13.bots.storable.insta

import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.StorableBotProcessStates
import ds13.bots.storable.insta.storage._
import ds13.bots.storable.insta.storage.TraitEverFollowedListStorage
import ds13.bots.storable.insta.storage.TraitNotToUnFollowListStorage
import ds13.bots.storable.StorableBotProcessStates
import ds13.bots.TraitBotConfig
import ds13.bots.storable.insta.storage.TraitInstaStorage
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.insta.storage.TraitNotToFollowListStorage

class InstaBotWithLists(
  config: TraitBotConfig,
  logger: TraitBotProcessLogger)
    extends LoggedInInstaBot(config, logger) {

  var notToFollowListStorage: TraitNotToFollowListStorage = null

  var notToUnFollowListStorage: TraitNotToUnFollowListStorage = null

  var everFollowedListStorage: TraitEverFollowedListStorage = null

  override def catchedStep: Unit =
    state match {
      case StorableBotProcessStates.STATE_STORABLE_INIT =>
        super.catchedStep
        if (state == StorableBotProcessStates.STATE_STORABLE_INIT_FINISHED) {
          launchStorage.accountStorage.accountTypeStorage.storage match {
            case storage: TraitInstaStorage =>
              storage.getNotToFollowList(launchStorage.accountStorage)
                .fold(errFinish("Can't get not to follow list!")) { t1 =>
                  notToFollowListStorage = t1
                  storage.getNotToUnFollowList(launchStorage.accountStorage)
                    .fold(errFinish("Can't get not to unfollow list!")) { t2 =>
                      notToUnFollowListStorage = t2
                      storage.getEverFollowedList(launchStorage.accountStorage)
                        .fold(errFinish("Can't get ever followed list!")) { t3 =>
                          everFollowedListStorage = t3
                          debug("All lists succesfully received!")
                        }
                    }
                }
            case t =>
              errFinish("Can't get insta storage, wrong type " + t)
          }
        }
      case _ =>
        super.catchedStep
    }

}
