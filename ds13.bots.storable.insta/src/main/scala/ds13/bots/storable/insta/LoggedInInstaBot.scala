package ds13.bots.storable.insta

import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.insta.api.CheckpointLoggedOut

class LoggedInInstaBot(
  config: TraitBotConfig,
  logger: TraitBotProcessLogger)
    extends InstaBot(config, logger) {

  override def catchedStep: Unit =
    state match {
      case InstaBotProcessStates.STATE_INSTA_BOT_INIT_FINISHED =>
        setState(LoggedInInstaBotProcessStates.STATE_LOGIN)
      case LoggedInInstaBotProcessStates.STATE_LOGIN =>
        try {
          api.login
          debug("Successfully logged in!")
          setState(LoggedInInstaBotProcessStates.STATE_LOGIN_FINISHED)
        } catch {
          case e: CheckpointLoggedOut =>
            errFinish("Checkpoint for login needs to pass. I don't know how to avoid this one...")
        }
      case _ =>
        super.catchedStep
    }

}

