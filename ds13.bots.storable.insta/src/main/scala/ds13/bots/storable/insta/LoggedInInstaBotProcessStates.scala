package ds13.bots.storable.insta

object LoggedInInstaBotProcessStates {

  val STATE_LOGIN = InstaBotProcessStates.FIRST_FREE_STATE_INDEX

  val STATE_LOGIN_FINISHED = STATE_LOGIN + 1

  val FIRST_FREE_STATE_INDEX = STATE_LOGIN_FINISHED + 1

}