package ds13.bots.storable.insta.complexbot1

import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.insta.LoggedInInstaBotProcessStates
import ds13.bots.storable.insta.InstaBotWithLists
import ds13.bots.BotProcessStates
import ds13.insta.api.intrnl.model.lesfi.LESFICompiler
import ds13.insta.api.intrnl.model.providers.users.TraitUsersProvider
import ds13.insta.api.NotAuthorizedToViewUser
import scala.collection.mutable.ListBuffer
import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.UsersList
import org.json.JSONObject
import ds13.insta.api.NotFoundException

object ComplexBot1 {

  val STATE_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW = LoggedInInstaBotProcessStates.FIRST_FREE_STATE_INDEX

  val STATE_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW = STATE_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW + 1

  val STATE_MAIN_LOOP_PREPARE = STATE_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW + 1

  val STATE_UNFOLLOW = STATE_MAIN_LOOP_PREPARE + 1

  val STATE_FOLLOW = STATE_UNFOLLOW + 1

  val STATE_WAIT_LOOP = STATE_FOLLOW + 1

  val STATE_SMALL_PAUSE_FOLLOW = STATE_WAIT_LOOP + 1

  val STATE_SMALL_PAUSE_UNFOLLOW = STATE_SMALL_PAUSE_FOLLOW + 1

}

class ComplexBot1(
  config: TraitBotConfig,
  logger: TraitBotProcessLogger)
    extends InstaBotWithLists(
      config,
      logger) {

  var botState: String = null
   
  var nextIdSelfFollowings: Option[String] = None

  var nextIdSelfFollowers: Option[String] = None

  var prevState = BotProcessStates.STATE_UNKNOWN

  var provider: TraitUsersProvider = null

  var followedStageCount = 0

  var unfollowListOpt: Option[UsersList] = None

  var unfollowListIndex = 0

  var summaryUsersFollowedCount = 0

  var summaryUsersUnfollowedCount = 0

  var toUnfollow = ListBuffer[User]()

  override def catchedStep: Unit =
    state match {
      case LoggedInInstaBotProcessStates.STATE_LOGIN_FINISHED =>
        provider = new LESFICompiler().parseProvider(api, config.getString(ComplexBot1Config.LESFI)).get
        if (config.getBoolean(ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW))
          setState(ComplexBot1.STATE_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW)
        else if (config.getBoolean(ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW))
          setState(ComplexBot1.STATE_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW)
        else
          setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE)

      case ComplexBot1.STATE_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW =>
        debug("Fill self followings to non follow and nont unfollow list.")
        val usersList = api.getSelfFollowings(nextIdSelfFollowings)
        val ids = usersList.users.map(_.id)
        debug("I take " + ids.length + " self followings")
        everFollowedListStorage.add(ids)
        notToFollowListStorage.add(ids)
        if (config.getBoolean(ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW)) notToUnFollowListStorage.add(ids)
        usersList.nextMaxIdOpt.fold {
          if (config.getBoolean(ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW)) {
            setState(ComplexBot1.STATE_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW)
          } else {
            setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE)
          }
        }(t => nextIdSelfFollowings = Some(t))

      case ComplexBot1.STATE_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW =>
        debug("Fill self followers to non follow list.")
        val usersList = api.getSelfFollowers(nextIdSelfFollowers)
        val ids = usersList.users.map(_.id)
        debug("I take " + ids.length + " self followers")
        notToFollowListStorage.add(ids)
        usersList.nextMaxIdOpt.fold(setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE))(t => nextIdSelfFollowers = Some(t))

      case ComplexBot1.STATE_MAIN_LOOP_PREPARE =>
        debug("Main loop router started.")
        var isFirst = false
        if (prevState == BotProcessStates.STATE_UNKNOWN) {
          isFirst = true
          debug("Previous loop state is unknown.")
          prevState = if (config.getBoolean(ComplexBot1Config.IS_FIRST_FOLLOW)) ComplexBot1.STATE_UNFOLLOW else ComplexBot1.STATE_FOLLOW
          debug("Set prev state to: " + prevState + " - " + (if (config.getBoolean(ComplexBot1Config.IS_FIRST_FOLLOW)) "unfollow" else "follow") + ".")
        }
        prevState match {
          case ComplexBot1.STATE_UNFOLLOW =>
            debug("Previous loop state unknown or unfollow")
            debug("Prepare to follow loop....")
            if (provider.hasNext) {
              followedStageCount = 0
              setState(if (isFirst) ComplexBot1.STATE_FOLLOW else ComplexBot1.STATE_WAIT_LOOP)
              prevState = ComplexBot1.STATE_FOLLOW
            } else okFinish("Provider empty - all finnished!")
          case ComplexBot1.STATE_FOLLOW =>
            debug("Previous loop state unknown or follow")
            debug("Prepare to unfollow loop....")
            setState(if (isFirst) ComplexBot1.STATE_UNFOLLOW else ComplexBot1.STATE_WAIT_LOOP)
            prevState = ComplexBot1.STATE_UNFOLLOW
            updateUnfollowList(None)
          case _ =>
            errFinish("Uknown loop state: " + prevState)
        }

      case ComplexBot1.STATE_FOLLOW | ComplexBot1.STATE_SMALL_PAUSE_FOLLOW =>
        debug("Follow state works now.")
        if (followedStageCount < config.getInt(ComplexBot1Config.FOLLOW_LIMIT)) {
          provider.fold {
            debug("Provider has no more users...")
            isLoopFinish
          } { user =>
            debug("User taken from provider: " + user.username + " with id " + user.id)
            var filtered = false;

            if (notToFollowListStorage.contains(user.id)) {
              debug("User can't be followed because in not followed list: " + user.username + " with id " + user.id)
              filtered = true;

            }

            if (!filtered && user.isPrivate && !config.getBoolean(ComplexBot1Config.IS_FOLLOW_TO_PRIVATE)) {
              debug("User can't be followed because not to follow private users option enabled: " + user.username + " with id " + user.id)
              filtered = true;
            }

            if (!filtered && config.getBoolean(ComplexBot1Config.IS_FOLLOWERS_COUNT_LIMIT) || config.getBoolean(ComplexBot1Config.IS_FOLLOWING_COUNT_LIMIT)) {
              val info = api.getUserInfo(user.id)
              if (config.getBoolean(ComplexBot1Config.IS_FOLLOWERS_COUNT_LIMIT) && info.followerCount.get >= config.getInt(ComplexBot1Config.FOLLOWERS_COUNT_LIMIT)) {
                debug("User can't be followed because filtered by followers count limit: " + user.username +
                  " with id " + user.id + " have followers " + info.followerCount + " and filtered by limit: " + config.getInt(ComplexBot1Config.FOLLOWERS_COUNT_LIMIT))
                filtered = true
              }
              if (!filtered && config.getBoolean(ComplexBot1Config.IS_FOLLOWING_COUNT_LIMIT) && info.followingCount.get >= config.getInt(ComplexBot1Config.FOLLOWING_COUNT_LIMIT)) {
                debug("User can't be followed because filtered by followings count limit: " + user.username +
                  " with id " + user.id + " have followings " + info.followingCount + " and filtered by limit: " + config.getInt(ComplexBot1Config.FOLLOWING_COUNT_LIMIT))
                filtered = true
              }
            }

            if (filtered) {
              setState(ComplexBot1.STATE_SMALL_PAUSE_FOLLOW)
            } else {
              if (config.getBoolean(ComplexBot1Config.IS_LIKE_LASTPOST_BEFORE_FOLLOW)) {
                debug("It needs to like one post before follow... Try to get feed for " + user.id)
                if (user.isPrivate)
                  debug("Can't get feeds from private user. In this case I can't like his posts")
                else
                  try {
                    val feed = api.getUserFeed(user.id)
                    debug("User have last " + feed.items.length + " feed items")
                    if (feed.items.length > 0) {
                      debug("Try to find post with minimum likes...")
                      val items = feed.items.sortBy(_.likeCount)
                      val item = items(0)
                      debug("Post have likes count: " + item.likeCount)
                      debug("Try to like post with id " + item.pk)
                      api.like(feed.items.last.pk)
                    }
                  } catch {
                    case e: NotAuthorizedToViewUser =>
                      debug("NotAuthorizedToViewUser exception prevented for user " + user)
                  }
              }

              debug("Try to follow user: " + user.toString)
              val info = api.follow(user.id)
              everFollowedListStorage.add(user.id)
              notToFollowListStorage.add(user.id)
              followedStageCount += 1
              summaryUsersFollowedCount += 1
              toUnfollow += user
              debug("User: " + user.username + " with id " + user.id + "  - followed successfully")
              printStat
              setState(ComplexBot1.STATE_FOLLOW)
            }
          }
        } else {
          debug("Follow limit per stage " + followedStageCount + " reached.")
          isLoopFinish
        }

      case ComplexBot1.STATE_UNFOLLOW | ComplexBot1.STATE_SMALL_PAUSE_UNFOLLOW =>
        debug("Unfollow state works now.")
        if (toUnfollow.isEmpty) {
          unfollowListOpt.fold(errFinish("Somthig went wrong, because unfollow list opt is null!")) { unfollowList =>
            while (if (unfollowListIndex >= unfollowList.users.length) {
              unfollowList.nextMaxIdOpt.fold {
                isLoopFinish
                false
              } { nextMaxId =>
                updateUnfollowList(Some(nextMaxId))
                unfollowListOpt match {
                  case Some(unfollowList) =>
                    if (unfollowList.users.length > 0) {
                      unfollowListOpt.foreach(unfollowUserFromUserList)
                      false
                    } else true
                  case _ => false
                }
              }
            } else {
              unfollowUserFromUserList(unfollowList)
              false
            }) {}
          }
        } else {
          val user = toUnfollow.head
          unfollowUser(user)
          toUnfollow -= user
        }

      case ComplexBot1.STATE_WAIT_LOOP =>
        debug("Waiting between loop stages finished")
        setState(prevState)

      case _ =>
        super.catchedStep
    }

  def updateUnfollowList(nextMaxIdOpt: Option[String]) = {
    unfollowListOpt = Some(api.getSelfFollowings(nextMaxIdOpt))
    unfollowListIndex = 0
  }

  def unfollowUserFromUserList(unfollowList: UsersList) = {
    unfollowUser(unfollowList.users(unfollowListIndex))
    unfollowListIndex += 1
  }

  def unfollowUser(user: User) = {
    if (notToUnFollowListStorage.contains(user.id)) {
      debug("User can't be unfollowed because in not unfollowed list: " + user.username + " with id " + user.id)
      setState(ComplexBot1.STATE_SMALL_PAUSE_UNFOLLOW)
    } else {
      debug("Try to unfollow user: " + user.toString)
      try {
        api.unfollow(user.id)
        summaryUsersUnfollowedCount += 1
        debug("User: " + user.username + " with id " + user.id + "  - unfollowed successfully")
      } catch {
        case e: NotFoundException =>
          debug("Couldn't unfollow to user: " + user.username + " with id " + user.id + " - not found returned!")
      }
      printStat
      setState(ComplexBot1.STATE_UNFOLLOW)
    }
  }

  def isLoopFinish =
    if (config.getBoolean(ComplexBot1Config.IS_LOOP)) setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE) else {
      printStat
      okFinish("Finished!")
    }

  def printStat: Unit = {
    debug("Summay   followed: " + summaryUsersFollowedCount)
    debug("Summay unfollowed: " + summaryUsersUnfollowedCount)
  }

  override def getPause =
    state match {
      case ComplexBot1.STATE_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW => 1000
      case ComplexBot1.STATE_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW => 1000
      case ComplexBot1.STATE_MAIN_LOOP_PREPARE => 1000
      case ComplexBot1.STATE_SMALL_PAUSE_FOLLOW => 1000
      case ComplexBot1.STATE_UNFOLLOW => 40000
      case ComplexBot1.STATE_FOLLOW => 90000
      case ComplexBot1.STATE_SMALL_PAUSE_UNFOLLOW => 1000
      case ComplexBot1.STATE_WAIT_LOOP => config.getLong(ComplexBot1Config.TIME_BETWEEN_LOOP)
      case _ => 1000
    }

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("provider", provider.toJSON.get)
    })
    
    
  override def step {
    if(botState == null) updateState
    super.step
  }
  
  def isStateInitialized = synchronized {
    
  }

  def updateState = synchronized {
    botState = config.getString(ComplexBot1Config.LESFI) 
  }

  override def getState: Any = synchronized {
    botState
  }

}