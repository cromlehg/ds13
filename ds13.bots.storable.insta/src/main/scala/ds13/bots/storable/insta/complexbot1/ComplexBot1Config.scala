package ds13.bots.storable.insta.complexbot1

object ComplexBot1Config {

  val LESFI = "lesfi"

  val IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW = "isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow"

  val IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW = "isNeedToFillSelfFollowersAsNonToFollow"

  val FOLLOW_LIMIT = "followLimit"

  val IS_FIRST_FOLLOW = "isFirstFollow"

  val IS_LOOP = "isLoop"

  val TIME_BETWEEN_LOOP = "timeBetweenLoop"

  val IS_LIKE_LASTPOST_BEFORE_FOLLOW = "isLikeLastPostBeforeFollow"

  val IS_FOLLOW_TO_PRIVATE = "isFollowToPrivate"

  val IS_FOLLOWERS_COUNT_LIMIT = "isFollowersCountLimit"

  val IS_FOLLOWING_COUNT_LIMIT = "isFollowingsCountLimit"

  val FOLLOWERS_COUNT_LIMIT = "followersCountLimit"

  val FOLLOWING_COUNT_LIMIT = "followingsCountLimit"

}
