package ds13.bots.storable.insta.complexbot1

import ds13.bots.BotServiceFactory
import ds13.bots.BotSystem
import ds13.bots.storable.insta.InstaBotDescriptor
import ds13.common.Logger

class ComplexBot1Descriptor(
  system: BotSystem,
  id: Long,
  serviceFactory: BotServiceFactory,
  accountTypeId: Long,
  logger: Logger) // In some cases not thread safety, but in play it's Ok
    extends InstaBotDescriptor(
      system,
      id,
      serviceFactory,
      accountTypeId) {

  override def createBotLaunchHelper: ComplexBot1LaunchHelper =
    new ComplexBot1LaunchHelper(system, logger, serviceFactory, id, accountTypeId)

}