package ds13.bots.storable.insta.complexbot1

import ds13.bots.BotServiceFactory
import ds13.bots.BotSystem
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.insta.InstaBotLaunchHelper
import ds13.common.Logger

class ComplexBot1LaunchHelper(
  system: BotSystem,
  logger: Logger,
  serviceFactory: BotServiceFactory,
  descriptorId: Long,
  accountTypeId: Long)
    extends InstaBotLaunchHelper(
      system,
      logger,
      serviceFactory,
      descriptorId,
      accountTypeId) {

  override protected def createBotProcess(config: TraitBotConfig, logger: TraitBotProcessLogger): ComplexBot1 =
    new ComplexBot1(config, logger)

}