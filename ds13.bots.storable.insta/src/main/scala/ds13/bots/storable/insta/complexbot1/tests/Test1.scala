package ds13.bots.storable.insta.complexbot1.tests

import ds13.bots.BotConfig
import ds13.bots.BotStates
import ds13.bots.storable.ConfigProvider
import ds13.bots.storable.StorableBotSystem
import ds13.bots.storable.StorableBots
import ds13.bots.storable.insta.complexbot1.ComplexBot1Descriptor
import ds13.bots.storable.insta.storage.slick.mysql.InstaStorableBotMySQLSlickServiceFactory
import ds13.common.PrintAllLogger
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver
import ds13.bots.storable.insta.complexbot1.ComplexBot1Config

object Test1 extends App {

  val configProvider = new ConfigProvider

  val dbConfig: DatabaseConfig[MySQLDriver] = DatabaseConfig.forConfig("slick.dbs.default")

  val db = dbConfig.db

  val logger = PrintAllLogger()

  val botServiceFactory = new InstaStorableBotMySQLSlickServiceFactory(configProvider.path, dbConfig, db)

  val botSystem = new StorableBotSystem("InstaBotSystemV1")

  val complexBotDescriptor = new ComplexBot1Descriptor(botSystem, 1, botServiceFactory.copy, 1, logger)

  botSystem.addDescriptor(complexBotDescriptor)

  val descriptor = botSystem.getDescriptor(1)

  val launchHelper = descriptor.createBotLaunchHelper

  val config = new BotConfig(
    StorableBots.ACCOUNT_ID -> 1L,
    ComplexBot1Config.LESFI -> "@buzova86",
    ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW -> false,
    ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW -> false,
    ComplexBot1Config.FOLLOW_LIMIT -> 3000,
    ComplexBot1Config.IS_FIRST_FOLLOW -> true,
    ComplexBot1Config.IS_LOOP -> true,
    ComplexBot1Config.TIME_BETWEEN_LOOP -> 90000,
    ComplexBot1Config.IS_LIKE_LASTPOST_BEFORE_FOLLOW -> true,
    ComplexBot1Config.IS_FOLLOW_TO_PRIVATE -> false/*,
    ComplexBot1Config.LIKE_COUNT_LIMIT -> 50,
    ComplexBot1Config.PASS_USERS_WITH_LIKE_COUNT_LIMITED -> false*/)

  launchHelper.prepareProcess(config)

  launchHelper.sendMsg("start")

  while (launchHelper.launchStatus != BotStates.STATE_STOPPED)
    Thread.sleep(5000)

  launchHelper.sendMsg("remove")

  Thread.sleep(5000)

  db.close()

}