package ds13.bots.storable.insta.storage

case class AccountUserLink(
  val accountId: Long,
  val userId: Long)