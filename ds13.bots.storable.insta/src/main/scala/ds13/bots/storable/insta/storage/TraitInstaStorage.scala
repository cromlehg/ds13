package ds13.bots.storable.insta.storage

import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.TraitStorage

trait TraitInstaStorage extends TraitStorage {

  def getToFollowList(accountStorage: TraitAccountStorage): Option[TraitToFollowListStorage]

  def getEverFollowedList(accountStorage: TraitAccountStorage): Option[TraitEverFollowedListStorage]

  def getNotToFollowList(accountStorage: TraitAccountStorage): Option[TraitNotToFollowListStorage]

  def getNotToUnFollowList(accountStorage: TraitAccountStorage): Option[TraitNotToUnFollowListStorage]

  def addToFollowList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int

  def addEverFollowedList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int

  def addNotToFollowList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int

  def addNotToUnFollowList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int

  def addToFollowList(accountStorage: TraitAccountStorage, id: Long): Boolean

  def addEverFollowedList(accountStorage: TraitAccountStorage, id: Long): Boolean

}