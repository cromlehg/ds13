package ds13.bots.storable.insta.storage

import ds13.common.LoggerSupported

trait TraitListStorage extends LoggerSupported {

  def length: Long

  def add(id: Long): Boolean

  def add(id: Seq[Long]): Int

  def remove(id: Long): Boolean

  def contains(id: Long): Boolean

  def takeWhile(f: Long => Boolean): Unit
  
  //def refresh: Unit

}
