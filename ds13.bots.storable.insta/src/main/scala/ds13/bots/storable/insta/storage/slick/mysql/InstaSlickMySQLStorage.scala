package ds13.bots.storable.insta.storage.slick.mysql

import ds13.bots.storable.insta.storage.TraitEverFollowedListStorage
import ds13.bots.storable.insta.storage.TraitInstaStorage
import ds13.bots.storable.insta.storage.TraitNotToFollowListStorage
import ds13.bots.storable.insta.storage.TraitNotToUnFollowListStorage
import ds13.bots.storable.insta.storage.TraitToFollowListStorage
import ds13.bots.storable.insta.storage.slick.mysql.lists.InstaSlickMySQLEverFollowedListStorage
import ds13.bots.storable.insta.storage.slick.mysql.lists.InstaSlickMySQLNotToFollowListStorage
import ds13.bots.storable.insta.storage.slick.mysql.lists.InstaSlickMySQLNotToUnFollowListStorage
import ds13.bots.storable.insta.storage.slick.mysql.lists.InstaSlickMySQLToFollowListStorage
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.slick.mysql.SlickMySQLStorage
import ds13.common.Logger
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver

class InstaSlickMySQLStorage(
  logger: Logger,
  config: DatabaseConfig[MySQLDriver],
  db: slick.driver.MySQLDriver#Backend#Database)
    extends SlickMySQLStorage(
      logger,
      config,
      db)
    with TraitInstaStorage {

  override val dao = new InstaSlickMySQLDAO(logger, config, db)

  override def getToFollowList(accountStorage: TraitAccountStorage): Option[TraitToFollowListStorage] =
    Some(new InstaSlickMySQLToFollowListStorage(dao, accountStorage, solved(dao.getToFollowListByAccountId(accountStorage.account.id))))

  override def getEverFollowedList(accountStorage: TraitAccountStorage): Option[TraitEverFollowedListStorage] =
    Some(new InstaSlickMySQLEverFollowedListStorage(dao, accountStorage, solved(dao.getEverFollowedListByAccountId(accountStorage.account.id))))

  override def getNotToFollowList(accountStorage: TraitAccountStorage): Option[TraitNotToFollowListStorage] =
    Some(new InstaSlickMySQLNotToFollowListStorage(dao, accountStorage, solved(dao.getNotToFollowListByAccountId(accountStorage.account.id))))

  override def getNotToUnFollowList(accountStorage: TraitAccountStorage): Option[TraitNotToUnFollowListStorage] =
    Some(new InstaSlickMySQLNotToUnFollowListStorage(dao, accountStorage, solved(dao.getNotToUnFollowListByAccountId(accountStorage.account.id))))

  override def addToFollowList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int =
    solvedInt(dao.addToToFollowList(accountStorage.account.id, ids))

  override def addEverFollowedList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int =
    solvedInt(dao.addToEverFollowedList(accountStorage.account.id, ids))

  override def addNotToFollowList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int =
    solvedInt(dao.addToNotToFollowList(accountStorage.account.id, ids))

  override def addNotToUnFollowList(accountStorage: TraitAccountStorage, ids: Seq[Long]): Int =
    solvedInt(dao.addToNotToUnFollowList(accountStorage.account.id, ids))

  override def addToFollowList(accountStorage: TraitAccountStorage, id: Long): Boolean =
    solved(dao.addToToFollowList(accountStorage.account.id, id))

  override def addEverFollowedList(accountStorage: TraitAccountStorage, id: Long): Boolean =
    solved(dao.addToEverFollowedList(accountStorage.account.id, id))

}