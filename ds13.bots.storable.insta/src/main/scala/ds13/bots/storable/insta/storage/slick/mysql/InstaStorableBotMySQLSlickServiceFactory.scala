package ds13.bots.storable.insta.storage.slick.mysql

import ds13.bots.BotConfig
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotStorage
import ds13.bots.storable.storage.slick.mysql.StorableBotMySQLSlickServiceFactory
import ds13.common.Logger
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver

class InstaStorableBotMySQLSlickServiceFactory(
  path: String,
  dbConfig: DatabaseConfig[MySQLDriver],
  db: slick.driver.MySQLDriver#Backend#Database)
    extends StorableBotMySQLSlickServiceFactory(path, dbConfig, db) {

  override def copy =
    new InstaStorableBotMySQLSlickServiceFactory(path, dbConfig, db)

  override def createStorage(config: TraitBotConfig): TraitBotStorage =
    new InstaSlickMySQLStorage(config.get[Logger](BotConfig.LOGGER), dbConfig, db)

}