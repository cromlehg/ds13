package ds13.bots.storable.insta.storage.slick.mysql

import slick.driver.MySQLDriver.api._
import java.sql.Blob
import ds13.bots.storable.insta.storage.AccountUserLink

class ListsNotUnfollow(tag: Tag) extends Table[AccountUserLink](tag, "lists_not_unfollow") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId) <> (AccountUserLink.tupled, AccountUserLink.unapply)
}

class ListsNotToFollow(tag: Tag) extends Table[AccountUserLink](tag, "lists_not_to_follow") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId) <> (AccountUserLink.tupled, AccountUserLink.unapply)
}

class ListsEverFollowed(tag: Tag) extends Table[AccountUserLink](tag, "lists_ever_followed") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId) <> (AccountUserLink.tupled, AccountUserLink.unapply)
}

class ListsToFollow(tag: Tag) extends Table[AccountUserLink](tag, "lists_to_follow") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId) <> (AccountUserLink.tupled, AccountUserLink.unapply)
}
