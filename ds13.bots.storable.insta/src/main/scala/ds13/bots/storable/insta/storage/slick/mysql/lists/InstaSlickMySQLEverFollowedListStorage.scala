package ds13.bots.storable.insta.storage.slick.mysql.lists

import ds13.bots.storable.insta.storage.TraitEverFollowedListStorage
import ds13.bots.storable.insta.storage.slick.mysql.InstaSlickMySQLDAO
import ds13.bots.storable.storage.TraitAccountStorage
import scala.collection.mutable.ListBuffer

class InstaSlickMySQLEverFollowedListStorage(
  dao: InstaSlickMySQLDAO,
  accountStorage: TraitAccountStorage,
  list: Seq[Long])
    extends InstaSlickMySQLListStorage(
      dao,
      accountStorage,
      list)
    with TraitEverFollowedListStorage {

  override def add(id: Long): Boolean = {
    solved(dao.addToEverFollowedList(accountStorage.account.id, id))
    listBuffer += id
    true
  }

  override def remove(id: Long): Boolean = {
    solved(dao.removeFromEverFollowedList(accountStorage.account.id, id))
    listBuffer -= id
    true
  }

//  override def refresh: Unit =
//    listBuffer = ListBuffer() ++= solved(dao.getEverFollowedListByAccountId(accountStorage.account.id))

}
