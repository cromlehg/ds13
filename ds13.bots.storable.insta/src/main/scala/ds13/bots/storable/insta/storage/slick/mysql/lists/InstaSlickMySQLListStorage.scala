package ds13.bots.storable.insta.storage.slick.mysql.lists

import scala.collection.mutable.ListBuffer

import ds13.bots.storable.insta.storage.TraitListStorage
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.slick.mysql.ResultSolver
import ds13.bots.storable.storage.slick.mysql.SlickMySQLDAO

abstract class InstaSlickMySQLListStorage(
  val dao: SlickMySQLDAO,
  val accountStorage: TraitAccountStorage,
  val list: Seq[Long])
    extends TraitListStorage with ResultSolver {

  var listBuffer = ListBuffer() ++= list

  override val logger = accountStorage.logger

  override def takeWhile(f: Long => Boolean) = list takeWhile f

  override def length: Long = list.length

  override def contains(id: Long): Boolean = listBuffer contains id

  override def add(ids: Seq[Long]): Int = {
    var count = 0
    ids.foreach { id =>
      add(id)
      count += 1
    }
    count
  }

}
