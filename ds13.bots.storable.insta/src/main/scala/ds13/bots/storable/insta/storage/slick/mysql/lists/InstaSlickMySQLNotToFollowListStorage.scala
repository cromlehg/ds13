package ds13.bots.storable.insta.storage.slick.mysql.lists

import ds13.bots.storable.insta.storage.TraitNotToFollowListStorage
import ds13.bots.storable.insta.storage.slick.mysql.InstaSlickMySQLDAO
import ds13.bots.storable.storage.TraitAccountStorage

class InstaSlickMySQLNotToFollowListStorage(
  dao: InstaSlickMySQLDAO,
  accountStorage: TraitAccountStorage,
  list: Seq[Long])
    extends InstaSlickMySQLListStorage(
      dao,
      accountStorage,
      list)
    with TraitNotToFollowListStorage {

  override def add(id: Long): Boolean = {
    solved(dao.addToNotToFollowList(accountStorage.account.id, id))
    listBuffer += id
    true
  }

  override def remove(id: Long): Boolean = {
    solved(dao.removeFromNotToFollowList(accountStorage.account.id, id))
    listBuffer -= id
    true
  }

}
