package ds13.bots.storable.insta.storage.slick.mysql.lists

import ds13.bots.storable.insta.storage.TraitNotToUnFollowListStorage
import ds13.bots.storable.storage.slick.mysql.SlickMySQLAccountStorage
import ds13.bots.storable.insta.storage.slick.mysql.InstaSlickMySQLDAO
import ds13.bots.storable.storage.TraitAccountStorage

class InstaSlickMySQLNotToUnFollowListStorage(
  dao: InstaSlickMySQLDAO,
  accountStorage: TraitAccountStorage,
  list: Seq[Long])
    extends InstaSlickMySQLListStorage(
      dao,
      accountStorage,
      list)
    with TraitNotToUnFollowListStorage {

  override def add(id: Long): Boolean = {
    solved(dao.addToNotToUnFollowList(accountStorage.account.id, id))
    listBuffer += id
    true
  }

  override def remove(id: Long): Boolean = {
    solved(dao.removeFromNotToUnFollowList(accountStorage.account.id, id))
    listBuffer -= id
    true
  }

}
