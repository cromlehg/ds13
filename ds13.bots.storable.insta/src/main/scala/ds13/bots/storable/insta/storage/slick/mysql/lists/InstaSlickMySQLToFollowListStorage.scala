package ds13.bots.storable.insta.storage.slick.mysql.lists

import ds13.bots.storable.insta.storage.TraitToFollowListStorage
import ds13.bots.storable.insta.storage.slick.mysql.InstaSlickMySQLDAO
import ds13.bots.storable.storage.TraitAccountStorage

class InstaSlickMySQLToFollowListStorage(
  dao: InstaSlickMySQLDAO,
  accountStorage: TraitAccountStorage,
  list: Seq[Long])
    extends InstaSlickMySQLListStorage(
      dao,
      accountStorage,
      list)
    with TraitToFollowListStorage {

  override def add(id: Long): Boolean = {
    solved(dao.addToToFollowList(accountStorage.account.id, id))
    listBuffer += id
    true
  }

  override def remove(id: Long): Boolean = {
    solved(dao.removeFromToFollowList(accountStorage.account.id, id))
    listBuffer -= id
    true
  }

}
