name := "bots.storable"

organization := "ds13"

version := "0.2-SNAPSHOT"

scalaVersion := "2.11.8"

resolvers += "DS13" at "http://maven.siamway.ru/"

libraryDependencies ++= Seq(
  "ds13" %% "bots" % "0.2-SNAPSHOT",
  "mysql" % "mysql-connector-java" % "6.0.6",
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0",
  "org.slf4j" % "slf4j-nop" % "1.7.25")

val resolver = Resolver.ssh("DS13_Publish", "maven.siamway.ru", "/var/www/vhosts/maven.siamway.ru/httpdocs") withPermissions ("0644")

publishTo := Some(resolver as ("maven"))

publishArtifact in (packageSrc) := true

publishArtifact in (packageDoc) := true
