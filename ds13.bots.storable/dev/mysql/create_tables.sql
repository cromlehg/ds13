CREATE TABLE IF NOT EXISTS users (
  id                        SERIAL PRIMARY KEY
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE account_types (
  id                        SERIAL PRIMARY KEY,
  name                      VARCHAR(100) NOT NULL UNIQUE,
  version                   VARCHAR(100) NOT NULL UNIQUE,
  properties                TEXT,
  description               TEXT
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE accounts (
  id                        SERIAL PRIMARY KEY,
  owner_id                  BIGINT UNSIGNED NOT NULL,
  account_type_id           BIGINT UNSIGNED NOT NULL,
  properties                TEXT
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE robots (
  id                        SERIAL PRIMARY KEY,
  account_type_id           BIGINT UNSIGNED NOT NULL,
  name                      VARCHAR(100) NOT NULL UNIQUE,
  version                   VARCHAR(100) NOT NULL UNIQUE,
  properties                TEXT,
  description               TEXT
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE launches (
  id                        SERIAL PRIMARY KEY,
  account_id                BIGINT UNSIGNED NOT NULL,
  robot_id                  BIGINT UNSIGNED NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE proxies (
  id                        SERIAL PRIMARY KEY,
  ip                        VARCHAR(16) NOT NULL,
  auth_type                 INT,
  port                      INT UNSIGNED,
  login                     VARCHAR(100),
  pass                      VARCHAR(100)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE proxy_assigns (
  id                        SERIAL PRIMARY KEY,
  proxy_id                  BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED,
  account_id                BIGINT UNSIGNED,
  launch_id                 BIGINT UNSIGNED
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_not_unfollow (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_not_to_follow (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_ever_followed (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_to_follow (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

