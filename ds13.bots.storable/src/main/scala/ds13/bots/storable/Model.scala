package ds13.bots.storable

import ds13.common.TraitId

case class User(
    override val id: Long) extends TraitId
  

case class AccountType(
  override val id: Long,
  val name: String,
  val version: String,
  val properties: Option[String],
  val description: Option[String]) extends TraitId

case class Account(
  override val id: Long,
  val ownerId: Long,
  val accountTypeId: Long,
  val properties: Option[String]) extends TraitId

case class Robot(
  override val id: Long,
  val accountTypeId: Long,
  val name: String,
  val version: String,
  val properties: Option[String],
  val description: Option[String]) extends TraitId

case class Launch(
  override val id: Long,
  val accountId: Long,
  val robotId: Long) extends TraitId

case class Proxy(
  override val id: Long,
  val ip: String,
  val authType: Int,
  val port: Option[Int],
  val login: Option[String],
  val pass: Option[String]) extends TraitId

case class ProxyAssign(
  override val id: Long,
  val proxyId: Long,
  val userId: Option[Long],
  val accountId: Option[Long],
  val launchId: Option[Long]) extends TraitId