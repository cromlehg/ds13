package ds13.bots.storable

import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.AbstractStateMachineBotProcess
import ds13.bots.BotProcessStates
import ds13.bots.storable.storage.TraitStorage
import ds13.bots.storable.storage.TraitLaunchStorage

// TODO: Should move to storable bots 
object StorableBots {

  val STORAGE = "storage"

  val USER_ID = "user_id"
  
  val ACCOUNT_ID = "account_id"

  val DESCRIPTOR_ID = "descriptor_id"

  val LAUNCH_STORAGE = "launch_storage"

}

class StorableBot(
  config: TraitBotConfig,
  logger: TraitBotProcessLogger)
    extends AbstractStateMachineBotProcess(config, logger) {

  setState(StorableBotProcessStates.STATE_STORABLE_INIT)

  var launchStorage: TraitLaunchStorage = null

  override def catchedStep: Unit =
    state match {
      case StorableBotProcessStates.STATE_STORABLE_INIT =>
        debug("Start initialization for launch with id  " + id)
        debug("Try to get launch storage...")
        config.getOpt[TraitLaunchStorage](StorableBots.LAUNCH_STORAGE).fold {
          errFinish("Couldn't get launch storage from config!")
        } { t =>
          launchStorage = t
          debug("Launch storage successfully received!")
          setState(StorableBotProcessStates.STATE_STORABLE_INIT_FINISHED)
        }
      case unknwonState =>
        errFinish("Unknown state: " + unknwonState)
    }

}
