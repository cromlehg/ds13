package ds13.bots.storable

import ds13.bots.AbstractBotDescriptor
import ds13.bots.BotServiceFactory
import ds13.bots.BotSystem

abstract class StorableBotDescriptor(
  system: BotSystem,
  id: Long,
  serviceFactory: BotServiceFactory,
  val accountTypeId: Long)
    extends AbstractBotDescriptor(
      system,
      id,
      serviceFactory) {

  override def createBotLaunchHelper: StorableBotLaunchHelper

}