package ds13.bots.storable

import ds13.bots.CommmonImplicits.appContext
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.AbstractStateMachineBotProcess
import ds13.bots.BotProcessStates
import ds13.bots.storable.storage.TraitStorage
import ds13.bots.BotSystem
import ds13.common.Logger
import ds13.bots.BotServiceFactory
import ds13.bots.AbstractBotLaunchHelper
import ds13.bots.BotConfig
import ds13.bots.storable.storage.slick.mysql.ResultSolver
import ds13.bots.ControlMessage
import ds13.bots.Remove
import ds13.bots.storable.storage.TraitLaunchStorage

abstract class StorableBotLaunchHelper(
  system: BotSystem,
  logger: Logger,
  serviceFactory: BotServiceFactory,
  descriptorId: Long,
  accountTypeId: Long)
    extends AbstractBotLaunchHelper(
      system,
      logger,
      serviceFactory) with ResultSolver {

  var launchStorageOpt: Option[TraitLaunchStorage] = None

  var processLogger: TraitBotProcessLogger = null

  override def sendMsg(msg: ControlMessage): Unit =
    launchStorageOpt.foreach { launchStorage =>
      super.sendMsg(msg)
      msg match {
        case Remove() =>
          processLogger.dispose
          launchStorage.remove
        case _ =>
      }
    }

  override def prepareProcess(config: TraitBotConfig): Option[Long] = {
    processLogger = config.getOpt[TraitBotProcessLogger](BotConfig.LOGGER).getOrElse(serviceFactory.createLogger(config))
    config + (BotConfig.LOGGER, processLogger)
    val asbtractStorage = serviceFactory.createStorage(config)
    config + (StorableBots.STORAGE, asbtractStorage)
    config + (StorableBots.DESCRIPTOR_ID, descriptorId)
    val storage = asbtractStorage.asInstanceOf[TraitStorage]
    val accountId = config.getLong(StorableBots.ACCOUNT_ID)

    (for {
      accountStorage <- storage.getAccountStorageById(accountId)
        .toRight("Can't get account storage for id " + accountId + " and account type " + accountTypeId).right
      robotStorage <- storage.getRobotStorageById(descriptorId)
        .toRight("Can't get robot storage for " + descriptorId).right
      launchStorage <- storage.createLaunchStorage(-1, accountStorage, robotStorage)
        .toRight("Can't create launch storage for account id " + accountStorage.account.id + " and robot id " + robotStorage.robot.id).right
    } yield launchStorage).fold({ errMsg =>
      err(errMsg)
      None
    }, { launchStorage =>
      id = launchStorage.launch.id
      launchStorageOpt = Some(launchStorage)
      config + (BotConfig.ID, id)
      processLogger.update(config)
      config + (StorableBots.LAUNCH_STORAGE, launchStorage)
      super.prepareProcess(config)
    })

  }

  protected def createBotProcess(config: TraitBotConfig, logger: TraitBotProcessLogger): StorableBot

}