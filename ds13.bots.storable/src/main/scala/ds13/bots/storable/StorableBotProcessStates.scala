package ds13.bots.storable

import ds13.bots.BotProcessStates

object StorableBotProcessStates {

  val STATE_STORABLE_INIT = BotProcessStates.FIRST_FREE_STATE_INDEX

  val STATE_STORABLE_INIT_FINISHED = STATE_STORABLE_INIT + 1

  val FIRST_FREE_STATE_INDEX = STATE_STORABLE_INIT_FINISHED + 1

}