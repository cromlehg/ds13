package ds13.bots.storable

import ds13.bots.BotServiceFactory

class StorableBotServiceFactory(
  path: String)
    extends BotServiceFactory(path) {

  override def copy =
    new StorableBotServiceFactory(path)

}