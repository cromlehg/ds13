package ds13.bots.storable.storage

import ds13.common.LoggerSupported
import ds13.bots.storable.Account

trait TraitAccountStorage extends LoggerSupported {

  val userStorage: TraitUserStorage

  val accountTypeStorage: TraitAccountTypeStorage

  val account: Account
  
  def getProxy: Option[ds13.bots.storable.Proxy]

}

