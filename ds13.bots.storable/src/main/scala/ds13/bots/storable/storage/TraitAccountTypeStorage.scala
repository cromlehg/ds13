package ds13.bots.storable.storage

import ds13.common.LoggerSupported
import ds13.bots.storable.AccountType

trait TraitAccountTypeStorage extends LoggerSupported {

  val storage: TraitStorage

  val accountType: AccountType

}

