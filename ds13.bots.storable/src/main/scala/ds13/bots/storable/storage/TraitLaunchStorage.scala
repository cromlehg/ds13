package ds13.bots.storable.storage

import ds13.common.LoggerSupported
import ds13.bots.storable.Launch

trait TraitLaunchStorage extends LoggerSupported {

  val accountStorage: TraitAccountStorage

  val robotStorage: TraitRobotStorage

  val launch: Launch

  def remove: Unit

}

