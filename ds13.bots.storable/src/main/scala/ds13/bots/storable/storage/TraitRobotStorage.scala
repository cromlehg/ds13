package ds13.bots.storable.storage

import ds13.common.LoggerSupported
import ds13.bots.storable.Robot

trait TraitRobotStorage extends LoggerSupported {

  val storage: TraitStorage

  val robot: Robot

}

