package ds13.bots.storable.storage

import ds13.common.LoggerSupported
import ds13.bots.TraitBotStorage
import scala.concurrent.Future

trait TraitStorage extends LoggerSupported with TraitBotStorage {

  def getFutureUserStorageById(id: Long): Future[Option[TraitUserStorage]]

  def getFutureAccountTypeStorageById(id: Long): Future[Option[TraitAccountTypeStorage]]

  def getFutureAccountStorageById(id: Long): Future[Option[TraitAccountStorage]]

  def getFutureRobotStorageById(id: Long): Future[Option[TraitRobotStorage]]

  def getFutureLaunchByAccountStorageAndRobotStorage(
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Future[Option[TraitLaunchStorage]]

  def createFutureLaunchStorage(
    id: Long,
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Future[Option[TraitLaunchStorage]]

  def getUserStorageById(id: Long): Option[TraitUserStorage]

  def getAccountTypeStorageById(id: Long): Option[TraitAccountTypeStorage]

  def getAccountStorageById(id: Long): Option[TraitAccountStorage]

  def getRobotStorageById(id: Long): Option[TraitRobotStorage]

  def getLaunchByAccountStorageAndRobotStorage(
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Option[TraitLaunchStorage]

  def createLaunchStorage(
    id: Long,
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Option[TraitLaunchStorage]

  def dispose

}