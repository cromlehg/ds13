package ds13.bots.storable.storage

import ds13.common.LoggerSupported
import ds13.bots.storable.User

trait TraitUserStorage extends LoggerSupported {

  val storage: TraitStorage

  val user: User

}