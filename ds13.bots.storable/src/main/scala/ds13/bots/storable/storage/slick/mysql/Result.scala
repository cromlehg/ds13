package ds13.bots.storable.storage.slick.mysql

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

import ds13.common.LoggerSupported

trait ResultSolver extends LoggerSupported {

  val solverTimeoutInSec = 100000

  def solved(f: => Future[Unit]): Unit =
    Await.result(f, solverTimeoutInSec second)

  def solved(f: => Future[Boolean]): Boolean =
    Await.result(f, solverTimeoutInSec second)

  def solved(f: => Future[Int]): Int =
    Await.result(f, solverTimeoutInSec second)

  def solvedInt(f: => Future[Int]): Int =
    Await.result(f, solverTimeoutInSec second)

  def solved[T](f: => Future[Seq[T]]): Seq[T] =
    Await.result(f, solverTimeoutInSec second)

  def solved[T](f: => Future[Option[T]]): Option[T] =
    Await.result(f, solverTimeoutInSec second)

}