package ds13.bots.storable.storage.slick.mysql

import ds13.bots.storable.storage.TraitUserStorage
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.TraitAccountTypeStorage
import ds13.bots.storable.Account

class SlickMySQLAccountStorage(
  val dao: SlickMySQLDAO,
  override val userStorage: TraitUserStorage,
  override val accountTypeStorage: TraitAccountTypeStorage,
  override val account: Account)
    extends TraitAccountStorage with ResultSolver {

  override val logger = userStorage.logger

  override def getProxy: Option[ds13.bots.storable.Proxy] =
    solved(dao.getProxy(this))

}

