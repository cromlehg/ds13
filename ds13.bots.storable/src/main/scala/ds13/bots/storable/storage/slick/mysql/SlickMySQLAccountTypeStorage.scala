package ds13.bots.storable.storage.slick.mysql

import ds13.bots.storable.storage.TraitAccountTypeStorage
import ds13.bots.storable.AccountType
import ds13.bots.storable.storage.TraitStorage

class SlickMySQLAccountTypeStorage(
  val dao: SlickMySQLDAO,
  override val storage: TraitStorage,
  override val accountType: AccountType)
    extends TraitAccountTypeStorage with ResultSolver {

  override val logger = storage.logger

}

