package ds13.bots.storable.storage.slick.mysql

import scala.concurrent.Future

import ds13.bots.CommmonImplicits.appContext
import ds13.common.Logger
import ds13.common.LoggerSupported
import ds13.bots.storable.Account
import ds13.bots.storable.AccountType
import ds13.bots.storable.Robot
import ds13.bots.storable.User
import ds13.bots.storable.Launch
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.TraitRobotStorage
import slick.jdbc.JdbcBackend.DatabaseDef

trait TraitSlickMySQLDAO extends LoggerSupported {

  val config: DatabaseConfig[MySQLDriver]

  val db: slick.driver.MySQLDriver#Backend#Database

  import config.driver.api._

  val sbusers = TableQuery[Users]

  val accountTypes = TableQuery[AccountTypes]

  val accounts = TableQuery[Accounts]

  val robots = TableQuery[Robots]

  val launches = TableQuery[Launches]

  val proxies = TableQuery[Proxies]

  val proxyAssigns = TableQuery[ProxyAssigns]

  def dispose = db.close

  def getProxy(accountStorage: TraitAccountStorage): Future[Option[ds13.bots.storable.Proxy]] = {
    val query = for {
      proxyAssigns <- proxyAssigns.filter(_.accountId === accountStorage.account.id)
      proxy <- proxies.filter(_.id === proxyAssigns.proxyId)
    } yield proxy
    db.run(query.result.headOption)
  }

  def getUserById(id: Long): Future[Option[User]] =
    db.run(sbusers.filter(_.id === id).result.headOption)

  def getAccountTypeById(id: Long): Future[Option[AccountType]] =
    db.run(accountTypes.filter(_.id === id).result.headOption)

  def getAccountUserAccountTypeByAccountId(id: Long): Future[Option[(Account, AccountType, User)]] = {
    val query = for {
      dbAccount <- accounts.filter(_.id === id)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
      dbUser <- sbusers.filter(_.id === dbAccount.ownerId)
    } yield (dbAccount, dbAccountType, dbUser)
    db.run(query.result.headOption)
  }

  def getRobotById(id: Long): Future[Option[Robot]] =
    db.run(robots.filter(_.id === id).result.headOption)

  def getLaunchByAccountIdAndRobotId(accountId: Long, robotId: Long): Future[Option[Launch]] =
    db.run(launches.filter(t => t.accountId === accountId && t.robotId === robotId).result.headOption)

  def createLaunch(id: Long, accountStorage: TraitAccountStorage, robotStorage: TraitRobotStorage): Future[Option[Launch]] =
    createLaunch(id, accountStorage.account.id, robotStorage.robot.id)

  def createLaunch(id: Long, accountId: Long, robotId: Long): Future[Option[Launch]] = {
    val query = for {
      launches <- (launches returning launches.map(_.id) into ((v, id) => v.copy(id = id))) +=
        new Launch(id, accountId, robotId)
    } yield launches
    db.run(query.transactionally) map (s => Some(new Launch(s.id, s.accountId, s.robotId)))
  }

  def removeLaunchById(id: Long) =
    db.run(launches.filter(_.id === id).delete.transactionally) map (_ == 1)

}

class SlickMySQLDAO(
  override val logger: Logger,
  val config: DatabaseConfig[MySQLDriver],
  val db: slick.driver.MySQLDriver#Backend#Database)
    extends TraitSlickMySQLDAO
    with LoggerSupported {

}
