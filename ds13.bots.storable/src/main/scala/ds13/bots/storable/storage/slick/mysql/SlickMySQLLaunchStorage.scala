package ds13.bots.storable.storage.slick.mysql

import ds13.bots.storable.storage.TraitRobotStorage
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.TraitLaunchStorage
import ds13.bots.storable.Launch

class SlickMySQLLaunchStorage(
  val dao: SlickMySQLDAO,
  override val accountStorage: TraitAccountStorage,
  override val robotStorage: TraitRobotStorage,
  override val launch: Launch)
    extends TraitLaunchStorage with ResultSolver {

  override val logger = accountStorage.logger

  override def remove: Unit =
    solved(dao.removeLaunchById(launch.id))

}

