package ds13.bots.storable.storage.slick.mysql

import ds13.bots.storable.storage.TraitRobotStorage
import ds13.bots.storable.storage.TraitStorage
import ds13.bots.storable.Robot

class SlickMySQLRobotStorage(
  val dao: SlickMySQLDAO,
  override val storage: TraitStorage,
  override val robot: Robot)
    extends TraitRobotStorage with ResultSolver {

  override val logger = storage.logger

}

