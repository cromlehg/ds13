package ds13.bots.storable.storage.slick.mysql

import scala.concurrent.Future

import ds13.bots.CommmonImplicits.appContext
import ds13.common.Logger
import ds13.bots.storable.storage.TraitStorage
import ds13.bots.storable.storage.TraitUserStorage
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.TraitAccountTypeStorage
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.TraitUserStorage
import ds13.bots.storable.storage.TraitAccountStorage
import ds13.bots.storable.storage.TraitRobotStorage
import ds13.bots.storable.storage.TraitLaunchStorage
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver

class SlickMySQLStorage(override val logger: Logger, val config: DatabaseConfig[MySQLDriver], val db: slick.driver.MySQLDriver#Backend#Database) extends TraitStorage with ResultSolver {

  val dao = new SlickMySQLDAO(logger, config, db)

  override def getFutureUserStorageById(id: Long): Future[Option[TraitUserStorage]] =
    dao.getUserById(id).map(_.map(t => new SlickMySQLUserStorage(dao, this, t)))

  override def getFutureAccountTypeStorageById(id: Long): Future[Option[TraitAccountTypeStorage]] =
    dao.getAccountTypeById(id).map(_.map(t => new SlickMySQLAccountTypeStorage(dao, this, t)))

  override def getFutureAccountStorageById(id: Long): Future[Option[TraitAccountStorage]] =
    dao.getAccountUserAccountTypeByAccountId(id).map(_.map { t =>
      new SlickMySQLAccountStorage(
        dao,
        new SlickMySQLUserStorage(dao, this, t._3),
        new SlickMySQLAccountTypeStorage(dao, this, t._2),
        t._1)
    })

  override def getFutureRobotStorageById(id: Long): Future[Option[TraitRobotStorage]] =
    dao.getRobotById(id).map(_.map(t => new SlickMySQLRobotStorage(dao, this, t)))

  override def getFutureLaunchByAccountStorageAndRobotStorage(
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Future[Option[TraitLaunchStorage]] =
    dao.getLaunchByAccountIdAndRobotId(
      accountStorage.account.id,
      robotStorage.robot.id).map(_.map { t =>
        new SlickMySQLLaunchStorage(
          dao,
          accountStorage,
          robotStorage,
          t)
      })

  override def getUserStorageById(id: Long): Option[TraitUserStorage] =
    solved(getFutureUserStorageById(id))

  override def getAccountTypeStorageById(id: Long): Option[TraitAccountTypeStorage] =
    solved(getFutureAccountTypeStorageById(id))

  override def getAccountStorageById(id: Long): Option[TraitAccountStorage] =
    solved(getFutureAccountStorageById(id))

  override def getRobotStorageById(id: Long): Option[TraitRobotStorage] =
    solved(getFutureRobotStorageById(id))

  override def getLaunchByAccountStorageAndRobotStorage(
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Option[TraitLaunchStorage] =
    solved(getFutureLaunchByAccountStorageAndRobotStorage(accountStorage, robotStorage))

  def createFutureLaunchStorage(
    id: Long,
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Future[Option[TraitLaunchStorage]] =
    dao.createLaunch(id, accountStorage, robotStorage).map(_.map { t =>
      new SlickMySQLLaunchStorage(
        dao,
        accountStorage,
        robotStorage,
        t)
    })

  override def createLaunchStorage(
    id: Long,
    accountStorage: TraitAccountStorage,
    robotStorage: TraitRobotStorage): Option[TraitLaunchStorage] =
    solved(createFutureLaunchStorage(id, accountStorage, robotStorage))

  override def dispose = dao.dispose

}