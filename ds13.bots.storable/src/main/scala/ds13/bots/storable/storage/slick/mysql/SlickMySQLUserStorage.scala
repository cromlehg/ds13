package ds13.bots.storable.storage.slick.mysql

import ds13.bots.storable.storage.TraitUserStorage
import ds13.bots.storable.storage.TraitStorage
import ds13.bots.storable.User

class SlickMySQLUserStorage(
  val dao: SlickMySQLDAO,
  override val storage: TraitStorage,
  override val user: User)
    extends TraitUserStorage with ResultSolver {

  override val logger = storage.logger

}

