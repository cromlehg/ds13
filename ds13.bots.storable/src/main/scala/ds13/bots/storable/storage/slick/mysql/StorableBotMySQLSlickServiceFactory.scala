package ds13.bots.storable.storage.slick.mysql

import ds13.bots.BotConfig
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotStorage
import ds13.bots.storable.StorableBotServiceFactory
import ds13.common.Logger
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver

class StorableBotMySQLSlickServiceFactory(
  path: String,
  val dbConfig: DatabaseConfig[MySQLDriver],
  val db: slick.driver.MySQLDriver#Backend#Database)
    extends StorableBotServiceFactory(path) {

  override def copy = new StorableBotMySQLSlickServiceFactory(path, dbConfig, db)

  override def createStorage(config: TraitBotConfig): TraitBotStorage =
    new SlickMySQLStorage(config.get[Logger](BotConfig.LOGGER), dbConfig, db)

}