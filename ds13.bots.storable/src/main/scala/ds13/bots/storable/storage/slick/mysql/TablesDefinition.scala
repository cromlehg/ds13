package ds13.bots.storable.storage.slick.mysql

import slick.driver.MySQLDriver.api._
import java.sql.Blob
import ds13.bots.storable.User
import ds13.bots.storable.AccountType
import ds13.bots.storable.Account
import ds13.bots.storable.Robot
import ds13.bots.storable.Launch
import ds13.bots.storable.Proxy
import ds13.bots.storable.ProxyAssign

class Users(tag: Tag) extends Table[User](tag, "users") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def * = id <> (User, User.unapply)
}

class AccountTypes(tag: Tag) extends Table[AccountType](tag, "account_types") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def version = column[String]("version")
  def properties = column[Option[String]]("properties")
  def description = column[Option[String]]("description")
  def * = (id, name, version, properties, description) <> (AccountType.tupled, AccountType.unapply)
}

class Accounts(tag: Tag) extends Table[Account](tag, "accounts") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def ownerId = column[Long]("owner_id")
  def accountTypeId = column[Long]("account_type_id")
  def properties = column[Option[String]]("properties")
  def * = (id, ownerId, accountTypeId, properties) <> (Account.tupled, Account.unapply)
}

class Robots(tag: Tag) extends Table[Robot](tag, "robots") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def accountTypeId = column[Long]("account_type_id")
  def name = column[String]("name")
  def version = column[String]("version")
  def properties = column[Option[String]]("properties")
  def description = column[Option[String]]("description")
  def * = (id, accountTypeId, name, version, properties, description) <> (Robot.tupled, Robot.unapply)
}

class Launches(tag: Tag) extends Table[Launch](tag, "launches") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def accountId = column[Long]("account_id")
  def robotId = column[Long]("robot_id")
  def * = (id, accountId, robotId) <> (Launch.tupled, Launch.unapply)
}

class Proxies(tag: Tag) extends Table[Proxy](tag, "proxies") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def ip = column[String]("ip")
  def authType = column[Int]("auth_type")
  def port = column[Option[Int]]("port")
  def login = column[Option[String]]("login")
  def pass = column[Option[String]]("pass")
  def * = (id, ip, authType, port, login, pass) <> (Proxy.tupled, Proxy.unapply)
}

class ProxyAssigns(tag: Tag) extends Table[ProxyAssign](tag, "proxy_assigns") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def proxyId = column[Long]("proxy_id")
  def userId = column[Option[Long]]("user_id")
  def accountId = column[Option[Long]]("account_id")
  def launchId = column[Option[Long]]("launch_id")
  def * = (id, proxyId, userId, accountId, launchId) <> (ProxyAssign.tupled, ProxyAssign.unapply)
}

