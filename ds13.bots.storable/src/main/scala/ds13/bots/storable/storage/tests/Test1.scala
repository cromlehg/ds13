package ds13.bots.storable.storage.tests

import ds13.bots.BotConfig
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.ConfigProvider
import ds13.bots.storable.StorableBot
import ds13.bots.storable.StorableBotDescriptor
import ds13.bots.storable.StorableBotLaunchHelper
import ds13.bots.storable.StorableBotProcessStates
import ds13.bots.storable.StorableBotSystem
import ds13.bots.storable.StorableBots
import ds13.bots.storable.storage.slick.mysql.StorableBotMySQLSlickServiceFactory
import ds13.common.PrintAllLogger
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver

object TestBotStates {

  val STATE_WORK = StorableBotProcessStates.FIRST_FREE_STATE_INDEX + 1

}

object Test1 extends App {

  val configProvider = new ConfigProvider

  val dbConfig: DatabaseConfig[MySQLDriver] = DatabaseConfig.forConfig("slick.dbs.default")

  val db = dbConfig.db

  val logger = PrintAllLogger()

  val botServiceFactory = new StorableBotMySQLSlickServiceFactory(configProvider.path, dbConfig, db)

  val botSystem = new StorableBotSystem("TestBotSystemV1")

  val testBotDescriptor = new StorableBotDescriptor(botSystem, 1, botServiceFactory.copy, 1) {

    override def createBotLaunchHelper: StorableBotLaunchHelper = {
      new StorableBotLaunchHelper(system, logger, serviceFactory, id, accountTypeId) {

        override def createBotProcess(config: TraitBotConfig, logger: TraitBotProcessLogger): StorableBot =
          new StorableBot(config, serviceFactory.createLogger(config)) {

            override def catchedStep: Unit =
              state match {
                case StorableBotProcessStates.STATE_STORABLE_INIT_FINISHED =>
                  setState(TestBotStates.STATE_WORK)
                case TestBotStates.STATE_WORK =>
                  println("I am successfully started!")
                case unknwonState =>
                  errFinish("Unknown state: " + unknwonState)
              }

          }

      }
    }

  }

  botSystem.addDescriptor(testBotDescriptor)

  val descriptor = botSystem.getDescriptor(1)

  val launchHelper = descriptor.createBotLaunchHelper

  val config = new BotConfig(BotConfig.ID -> 1L, StorableBots.USER_ID -> 1L)

  launchHelper.prepareProcess(config)

  launchHelper.sendMsg("start")

  Thread.sleep(5000)

  launchHelper.sendMsg("stop")

  Thread.sleep(2000)

  launchHelper.sendMsg("remove")

  db.close()

}
