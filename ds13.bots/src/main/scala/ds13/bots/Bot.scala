package ds13.bots

import java.util.concurrent.atomic.AtomicInteger

import scala.concurrent.Future

import CommmonImplicits.appContext
import akka.actor.Actor
import akka.actor.actorRef2Scala
import akka.event.Logging
import java.util.concurrent.atomic.AtomicBoolean

object BotStates {

  val STATE_READY = 0

  val STATE_STOPPED = STATE_READY + 1

  val STATE_IN_PROCESS = STATE_STOPPED + 1

  val STATE_PAUSED = STATE_IN_PROCESS + 1

  val STATE_IN_PAUSE_PROCESS = STATE_PAUSED + 1

  val STATE_IN_STOP_PROCESS = STATE_IN_PAUSE_PROCESS + 1

  val STATE_IN_REMOVE_PROCESS = STATE_IN_STOP_PROCESS + 1

  val STATE_REMOVED = STATE_IN_REMOVE_PROCESS + 1

  val STATE_ERROR = STATE_REMOVED + 1

  val STATE_GET_STATE = STATE_ERROR + 1

  val STATE_UNKNOWN = STATE_GET_STATE + 1

}

class Bot(private val process: TraitBotProcess) extends Actor {

  private var future: Future[_] = null

  private val state = new AtomicInteger(BotStates.STATE_READY)

  private val processState = new AtomicInteger(BotProcessStates.STATE_IDLE)

  private val log = Logging(context.system, this)

  private def processIdString =
    "Process " + process.id + ": "

  protected def err(msg: String) =
    log.error(processIdString + msg)

  protected def warn(msg: String) =
    log.warning(processIdString + msg)

  protected def debug(msg: String) =
    log.debug(processIdString + msg)

  private def getMsgName(msg: Any) =
    msg match {
      case Start()     => "Start"
      case Stop()      => "Stop"
      case Pause()     => "Pause"
      case Continue()  => "Continue"
      case Remove()    => "Remove"
      case GetStatus() => "GetStatus"
      case GetState()  => "GetState"
    }

  private def getStateName =
    state.get match {
      case BotStates.STATE_READY             => "ready"
      case BotStates.STATE_STOPPED           => "stopped"
      case BotStates.STATE_IN_PROCESS        => "working"
      case BotStates.STATE_PAUSED            => "paused"
      case BotStates.STATE_IN_STOP_PROCESS   => "stopping"
      case BotStates.STATE_IN_PAUSE_PROCESS  => "pausing"
      case BotStates.STATE_IN_REMOVE_PROCESS => "removing"
      case BotStates.STATE_REMOVED           => "removed"
    }

  private def preventNonAllowedStates(msg: Any, f: => Unit, allowedStates: Int*) = {
    if (allowedStates contains state.get) f
    else warn("Prevented message \"" + getMsgName(msg) + "\" for state \"" + getStateName + "\"!")
  }

  private def allowedStatesForMsg(msg: Any): Seq[Int] =
    msg match {
      case Start()     => Seq(BotStates.STATE_READY)
      case Stop()      => Seq(BotStates.STATE_READY, BotStates.STATE_PAUSED, BotStates.STATE_IN_PROCESS)
      case Pause()     => Seq(BotStates.STATE_IN_PROCESS)
      case Continue()  => Seq(BotStates.STATE_PAUSED)
      case Remove()    => Seq(BotStates.STATE_READY, BotStates.STATE_STOPPED, BotStates.STATE_IN_PROCESS, BotStates.STATE_PAUSED, BotStates.STATE_IN_STOP_PROCESS, BotStates.STATE_IN_PAUSE_PROCESS)
      case GetStatus() => Seq(BotStates.STATE_READY, BotStates.STATE_STOPPED, BotStates.STATE_IN_PROCESS, BotStates.STATE_PAUSED, BotStates.STATE_IN_STOP_PROCESS, BotStates.STATE_IN_PAUSE_PROCESS, BotStates.STATE_IN_REMOVE_PROCESS, BotStates.STATE_REMOVED)
      case GetState()  => Seq(BotStates.STATE_READY, BotStates.STATE_STOPPED, BotStates.STATE_IN_PROCESS, BotStates.STATE_PAUSED, BotStates.STATE_IN_STOP_PROCESS, BotStates.STATE_IN_PAUSE_PROCESS, BotStates.STATE_IN_REMOVE_PROCESS, BotStates.STATE_REMOVED)
    }

  private def preventWrnogMessages(msg: Any, f: => Unit) = {
    val msgName = getMsgName(msg)
    debug("Message " + msgName + "  in process...")
    preventNonAllowedStates(msg, f, allowedStatesForMsg(msg): _*)
    debug("Message " + msgName + " prepared.")
  }

  override def receive = {
    case msg: Start => preventWrnogMessages(msg, {
      state.set(BotStates.STATE_IN_PROCESS)
      future = Future {
        while (state.get != BotStates.STATE_IN_STOP_PROCESS || state.get != BotStates.STATE_IN_REMOVE_PROCESS)
          state.get match {
            case BotStates.STATE_IN_PROCESS =>
              while (if (!processState.compareAndSet(BotProcessStates.STATE_IDLE, BotProcessStates.STATE_IN_PROCESS)) {
                process.step
                if (process.isFinished) state.set(BotStates.STATE_STOPPED)
                processState.set(BotProcessStates.STATE_IDLE)
                false
              } else true) {}
              while (process match {
                case processWithPause: TraitBotProcessWithPause =>
                  if (!processState.compareAndSet(BotProcessStates.STATE_IDLE, BotProcessStates.STATE_IN_PROCESS)) {
                    Thread.sleep(processWithPause.pause)
                    processState.set(BotProcessStates.STATE_IDLE)
                    false
                  } else true
                case _ => true
              }) {}
            case BotStates.STATE_IN_PAUSE_PROCESS  => state set BotStates.STATE_PAUSED
            case BotStates.STATE_IN_STOP_PROCESS   => state set BotStates.STATE_STOPPED
            case BotStates.STATE_IN_REMOVE_PROCESS => state set BotStates.STATE_REMOVED
            case _                                 => Thread sleep 1000
          }
      }
      future onComplete {
        case _ =>
          state.get match {
            case BotStates.STATE_IN_REMOVE_PROCESS => state set BotStates.STATE_REMOVED
            case _                                 => state set BotStates.STATE_STOPPED
          }
      }
    })
    case msg: Stop      => preventWrnogMessages(msg, state set BotStates.STATE_IN_STOP_PROCESS)
    case msg: GetState  => preventWrnogMessages(msg, sender ! StateMessage(process.getState))
    case msg: Pause     => preventWrnogMessages(msg, state set BotStates.STATE_IN_PAUSE_PROCESS)
    case msg: Continue  => preventWrnogMessages(msg, state set BotStates.STATE_IN_PROCESS)
    case msg: Remove    => preventWrnogMessages(msg, state set BotStates.STATE_IN_REMOVE_PROCESS)
    case msg: GetStatus => preventWrnogMessages(msg, sender ! state.get)
  }

}
