package ds13.bots

import scala.collection.mutable.Map
import scala.reflect.ClassTag

trait TraitBotConfig {

  val propsMap: scala.collection.mutable.Map[String, Any]

  def +(key: String, value: Any) = {
    propsMap += (key -> value)
    this
  }

  def getString(key: String): String = get[String](key).toString

  def getInt(key: String): Int = get[Int](key)

  def getLong(key: String): Long = get[Long](key)

  def getBoolean(key: String): Boolean = get[Boolean](key)

  def get[T](key: String): T = propsMap(key).asInstanceOf[T]

  def getOpt[T: ClassTag](key: String): Option[T] = propsMap.get(key) match {
    case Some(t: T) => Some(t)
    case _ => None
  }

  def getOptString(key: String): Option[String] = getOpt(key)

  def getOptInt(key: String): Option[Int] = getOpt(key)

  def getOptLong(key: String): Option[Long] = getOpt(key)

  def getOptBoolean(key: String): Option[Boolean] = getOpt(key)

  def id = getLong(BotConfig.ID)

}

class BotConfig(val props: (String, Any)*) extends TraitBotConfig {

  override val propsMap = collection.mutable.Map(props: _*)

}

object BotConfig {

  val ID = "id"
  
  val STORAGE = "storage"
  
  val LOGGER = "logger"

}

