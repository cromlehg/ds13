package ds13.bots

trait TraitBotDescriptor {

  val system: BotSystem

  val id: Long

  val serviceFactory: BotServiceFactory

  def createBotLaunchHelper: TraitBotLaunchHelper

  // description page

}

abstract class AbstractBotDescriptor(
  override val system: BotSystem,
  override val id: Long,
  override val serviceFactory: BotServiceFactory)
    extends TraitBotDescriptor