package ds13.bots

import akka.actor.Props
import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout

import ds13.common.LoggerSupported
import ds13.common.Logger

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

import CommmonImplicits.appContext

trait TraitBotLaunchHelper extends LoggerSupported {

  val system: BotSystem

  val serviceFactory: BotServiceFactory

  var ref: ActorRef = null

  var id: Long = -1

  var botProcessLogger: TraitBotProcessLogger = null

  def sendMsg(msg: ControlMessage): Unit =
    msg match {
      case Remove() =>
        ref ! Remove
        system.idToLaunchHelpers.remove(id)
      case otherMsg =>
        debug("ActorBotSystem.sendMsg(" + id + ", " + msg.toString + "): sending message to actor...")
        ref ! otherMsg
        debug("ActorBotSystem.sendMsg(" + id + ", " + msg.toString + "): message has been sent to actor")
    }

  protected def createBotProcess(config: TraitBotConfig, logger: TraitBotProcessLogger): TraitBotProcess

  def prepareProcess(config: TraitBotConfig): Option[Long] = {
    id = config.id
    botProcessLogger = config.getOpt[TraitBotProcessLogger](BotConfig.LOGGER).getOrElse(serviceFactory.createLogger(config))
    ref = system.system.actorOf(Props(new Bot(createBotProcess(config, botProcessLogger))), name = id.toString)
    system.idToLaunchHelpers.put(id, this)
    debug("New actor reference has been created with id " + id + "!")
    Some(id)
  }

  def launchStatus: Int = {
    implicit val timeout = Timeout(10 seconds)
    val future = (ref ? GetStatus())
    future onComplete {
      case Success(result) => result
      case Failure(error) => BotStates.STATE_UNKNOWN
    }
    val result = Await result (future, timeout.duration)
    result.asInstanceOf[Int]
  }

  def sendMsg(msg: String): Unit =
    stringMsgToCommand(msg).fold(err("Unknown commad : \"" + msg + "\" for bot helper with id " + id))(t => sendMsg(t))

  def stringMsgToCommand(msg: String): Option[ControlMessage] =
    msg.toLowerCase match {
      case "start" => Some(Start())
      case "stop" => Some(Stop())
      case "pause" => Some(Pause())
      case "continue" => Some(Continue())
      case "remove" => Some(Remove())
      case _ => None
    }

}

abstract class AbstractBotLaunchHelper(
  override val system: BotSystem,
  override val logger: Logger,
  override val serviceFactory: BotServiceFactory)
    extends TraitBotLaunchHelper


