package ds13.bots

abstract class Message

abstract class ControlMessage

case class Start() extends ControlMessage {

  override def toString = "Start"

}

case class Stop() extends ControlMessage {

  override def toString = "Stop"

}

case class Pause() extends ControlMessage {

  override def toString = "Pause"

}

case class Continue() extends ControlMessage {

  override def toString = "Continue"

}

case class Remove() extends ControlMessage {

  override def toString = "Remove"

}

abstract class AskMessage extends ControlMessage

case class GetStatus() extends AskMessage {

  override def toString = "getStatus"

}

case class GetState() extends AskMessage {

  override def toString = "getState"

}

case class StateMessage(val state: Any) extends Message {

  override def toString = "state message"
  
}

abstract class StatusMessage extends Message

case class StatusProcessed() extends StatusMessage {

  override def toString = "statusProcessed"

}

case class StatusPaused() extends StatusMessage {

  override def toString = "statusPaused"

}

case class StatusStopped() extends StatusMessage

case class StatusInStopProcess() extends StatusMessage

case class StatusReady() extends StatusMessage

case class StatusUnknown() extends StatusMessage

abstract class ResultMessage extends Message
