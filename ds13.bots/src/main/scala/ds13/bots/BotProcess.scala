package ds13.bots

import ds13.common.LoggerSupported
import ds13.common.TraitJSON
import org.json.JSONObject

trait TraitBotProcess extends TraitJSON with TraitBotProcessLoggerSupported {

  val config: TraitBotConfig

  var isFinishedFlag = false

  def id = config.id

  def isFinished: Boolean = isFinishedFlag

  def step {}
  
  def getState: Any = Unit

}

abstract class AbstractBotProcess(
  override val config: TraitBotConfig,
  override val logger: TraitBotProcessLogger)
    extends TraitBotProcess

object BotProcessStates {

  val STATE_UNKNOWN = -1

  val STATE_IDLE = STATE_UNKNOWN + 1

  val STATE_IN_PROCESS = STATE_IDLE + 1

  val STATE_FINISHED = STATE_IN_PROCESS + 1

  val STATE_INIT = STATE_FINISHED + 1

  val STATE_INIT_FINISHED = STATE_INIT + 1

  val FIRST_FREE_STATE_INDEX = STATE_INIT_FINISHED + 1

}

trait TraitBotProcessWithPause extends TraitBotProcess {

  def pause: Long = 1000

}

abstract class AbstractBotProcessWithPause(config: TraitBotConfig, logger: TraitBotProcessLogger) extends AbstractBotProcess(config, logger)
