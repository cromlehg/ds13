package ds13.bots

import ds13.common.Logger
import ds13.common.PrintAllLogger

trait TraitBotProcessLogger extends Logger {

  def updateState(botProcess: TraitBotProcess) = {}

  def clean = {}

  def dispose = clean

  def update(config: TraitBotConfig) = {}

}

class PrintAllBotProcessLogger extends PrintAllLogger with TraitBotProcessLogger