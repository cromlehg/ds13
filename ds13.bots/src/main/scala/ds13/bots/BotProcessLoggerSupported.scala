package ds13.bots

import ds13.common.LoggerSupported

trait TraitBotProcessLoggerSupported extends LoggerSupported {

  override val logger: TraitBotProcessLogger

}