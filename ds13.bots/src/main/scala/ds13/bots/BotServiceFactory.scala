package ds13.bots

class BotServiceFactory(val path: String) {

  def createLogger(config: TraitBotConfig): TraitBotProcessLogger =
    new PrintAllBotProcessLogger()

  def createStorage(config: TraitBotConfig): TraitBotStorage =
    new TraitBotStorage {}

  def copy = new BotServiceFactory(path)

}