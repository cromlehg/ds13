package ds13.bots

import akka.actor.ActorSystem

class BotSystem(val name: String) {

  val system = ActorSystem(name)

  val idToLaunchHelpers = new java.util.concurrent.ConcurrentHashMap[Long, TraitBotLaunchHelper]()

  val idToDescriptors = new java.util.concurrent.ConcurrentHashMap[Long, TraitBotDescriptor]()

  def getDescriptor(id: Long): TraitBotDescriptor =
    synchronized(idToDescriptors.get(id))

  def addDescriptor(descr: TraitBotDescriptor): TraitBotDescriptor =
    synchronized {
      idToDescriptors.put(descr.id, descr)
    }

}