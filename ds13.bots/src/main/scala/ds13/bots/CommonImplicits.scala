package ds13.bots

import java.util.concurrent.Executors

import scala.concurrent.ExecutionContext

object CommmonImplicits {

  implicit val appContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(1000))

}