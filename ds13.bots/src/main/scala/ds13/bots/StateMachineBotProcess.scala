package ds13.bots

trait TraitStateMachineBotProcess extends TraitBotProcessWithPause {

  var state = BotProcessStates.STATE_IDLE

  def setState(state: Int) = {
    this.state = state
    debug("State changed to: " + state)
  }

  override def pause: Long = {
    val pauseTime = getPause
    debug("Time for pause: " + pauseTime)
    logger.updateState(this)
    pauseTime
  }

  def getPause: Long = 1000

  override def step: Unit =
    try catchedStep catch { case e: Throwable => catchExceptions(e) }

  def catchExceptions(e: Throwable): Unit = {
    err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
    routeException(e)
  }

  def routeException(e: Throwable) {
    throw e
  }

  def catchedStep: Unit = {}

  def errFinish(msg: String) {
    err(msg)
    setState(BotProcessStates.STATE_FINISHED)
    isFinishedFlag = true
  }

  def okFinish(msg: String) {
    debug(msg)
    setState(BotProcessStates.STATE_FINISHED)
    isFinishedFlag = true
  }

}

abstract class AbstractStateMachineBotProcess(config: TraitBotConfig, logger: TraitBotProcessLogger)
  extends AbstractBotProcessWithPause(config, logger)
  with TraitStateMachineBotProcess

class StateMachineBotSimpleExecutor(stateMachine: TraitStateMachineBotProcess) {

  def execute =
    while (!stateMachine.isFinished) {
      stateMachine.step
      Thread.sleep(stateMachine.pause)
    }

}