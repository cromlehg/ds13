name := "common"

organization := "ds13"

version := "0.2-SNAPSHOT"

scalaVersion := "2.11.8"

resolvers += "DS13" at "http://maven.siamway.ru/"

libraryDependencies ++= Seq(
  "org.json" % "json" % "20160810"
)

val resolver = Resolver.ssh("DS13_Publish", "maven.siamway.ru", "/var/www/vhosts/maven.siamway.ru/httpdocs") withPermissions ("0644")

publishTo := Some(resolver as ("maven"))

publishArtifact in (packageSrc) := true

publishArtifact in (packageDoc) := true
