name := "http"

organization := "ds13"

version := "0.2-SNAPSHOT"

scalaVersion := "2.11.8"

resolvers += "DS13" at "http://maven.siamway.ru/"

libraryDependencies ++= Seq(
  "ds13" %% "common" % "0.2-SNAPSHOT", 
  "org.apache.httpcomponents" % "httpclient" % "4.5.3",
  "org.json" % "json" % "20160810"
)

val resolver = Resolver.ssh("DS13_Publish", "maven.siamway.ru", "/var/www/vhosts/maven.siamway.ru/httpdocs") withPermissions ("0644")

publishTo := Some(resolver as ("maven"))

publishArtifact in (packageSrc) := true

publishArtifact in (packageDoc) := true
