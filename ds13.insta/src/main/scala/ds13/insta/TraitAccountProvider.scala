package ds13.insta

trait TraitAccountProvider {

  def next: Option[Account]

}