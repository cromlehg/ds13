package ds13.insta.api

class Error(val code: Int, val descr: Option[String])