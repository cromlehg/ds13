package ds13.insta.api

import ds13.http.TraitProxyProperties
import ds13.common.Logger
import ds13.insta.api.intrnl.InternalAPI2

class InstaAPIExecutor(
  logger: Logger,
  proxy: Option[TraitProxyProperties],
  appVersion: String,
  platform: String,
  deviceId: String,
  GUID: String,
  username: String,
  password: String)
    extends InternalAPI2(
      logger: Logger,
      proxy: Option[TraitProxyProperties],
      appVersion: String,
      platform: String,
      deviceId: String,
      GUID: String,
      username: String,
      password: String) {

}