package ds13.insta.api.intrnl.model

class FollowOperationInfo(
  val isFollowed: Boolean,
  val isRequested: Boolean,
  val isPrivate: Boolean,
  val isBlocked: Boolean)