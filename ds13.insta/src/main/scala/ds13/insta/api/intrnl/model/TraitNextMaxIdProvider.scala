package ds13.insta.api.intrnl.model

trait TraitNextMaxIdProvider {

  def getNextMaxId: Option[String]

}