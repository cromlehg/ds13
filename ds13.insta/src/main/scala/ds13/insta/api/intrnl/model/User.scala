package ds13.insta.api.intrnl.model

import org.json.JSONObject

import ds13.common.TraitJSON

class User(
  val username: String,
  val id: Long,
  val isPrivate: Boolean,
  val isVerified: Option[Boolean],
  val isUnpublished: Option[Boolean],
  val isFavorite: Option[Boolean],
  val profilePicId: Option[String],
  val hasAnonymousProfilePicture: Option[Boolean],
  val profilePicUrl: Option[String],
  val fullName: String,
  val allowContactsSync: Option[Boolean],
  val showFeedBizConversionIcon: Option[Boolean],
  val biography: Option[String],
  val isBusiness: Option[Boolean],
  val mediaCount: Option[Int],
  val followerCount: Option[Int],
  val followingCount: Option[Int],
  val canConvertToBusiness: Option[Boolean],
  val isNeedy: Option[Boolean],
  val geoMediaCount: Option[Int],
  val canSeeOrganicInsights: Option[Boolean])
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("username", username)
      accumulate("pk", id)
      accumulate("is_private", isPrivate)
      isVerified.foreach(t => accumulate("is_verified", t))
      isUnpublished.foreach(t => accumulate("is_unpublished", t))
      isFavorite.foreach(t => accumulate("is_favorite", t))
      accumulate("full_name", fullName)
      profilePicId.foreach(t => accumulate("profile_pic_id", t))
      hasAnonymousProfilePicture.foreach(t => accumulate("has_anonymous_profile_picture", t))
      profilePicUrl.foreach(t => accumulate("profile_pic_url", t))
      allowContactsSync.foreach(t => accumulate("allow_contacts_sync", t))
      showFeedBizConversionIcon.foreach(t => accumulate("show_feed_biz_conversion_icon", t))
      biography.foreach(t => accumulate("biography", t))
      isBusiness.foreach(t => accumulate("is_business", t))
      mediaCount.foreach(t => accumulate("media_count", t))
      followerCount.foreach(t => accumulate("follower_count", t))
      followingCount.foreach(t => accumulate("following_count", t))
      canConvertToBusiness.foreach(t => accumulate("can_convert_to_business", t))
      isNeedy.foreach(t => accumulate("is_needy", t))
      geoMediaCount.foreach(t => accumulate("geo_media_count", t))
      canSeeOrganicInsights.foreach(t => accumulate("can_see_organic_insights", t))
    })

}

