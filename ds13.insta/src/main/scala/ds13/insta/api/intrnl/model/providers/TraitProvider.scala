package ds13.insta.api.intrnl.model.providers

import ds13.insta.api.intrnl.model.User
import ds13.common.TraitJSON
import org.json.JSONObject
import ds13.insta.api.InstaAPIExecutor

trait TraitProvider[+M] extends TraitJSON {

  val name: String

  val api: InstaAPIExecutor

  def hasNext: Boolean

  def apply[T](f: M => T): T

  def fold[T](isEmpty: => T)(f: M => T): T =
    if (hasNext) apply(f) else isEmpty

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("name", name)
      getJSONState.foreach(s => accumulate("state", s))
    })

  def getJSONState: Option[JSONObject] = None

}