package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.InstaAPIExecutor

object CompositeUsersProvider {

  val NAME = "CompositeUsersProvider"

}

class CompositeUsersProvider(
  override val api: InstaAPIExecutor,
  override val providers: TraitUsersProvider*)
    extends TraitCompositeUsersProvider {

  override val name = CompositeUsersProvider.NAME

}