package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.InstaAPIExecutor
import ds13.insta.api.intrnl.model.EmptyFeed
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId
import ds13.insta.api.InstaAPIExecutor

object HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider {

  val NAME = "HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider"

}

class HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider(
  override val api: InstaAPIExecutor,
  val locationId: Long)
    extends TraitHashtagFeedItemsOwnersUsersProvider {

  override val name = HashtagFeedItemsOwnersUsersProvider.NAME

  override def getNextContainer(iapi: InstaAPIExecutor, nextMaxIdOpt: Option[String]): TraitUsersContainerWithNextMaxId =
    iapi.getLocationFeed(locationId, nextMaxIdOpt)

  override def getJSONState: Option[JSONObject] =
    Some(super.getJSONState.getOrElse(new JSONObject).accumulate("location_id", locationId))

}