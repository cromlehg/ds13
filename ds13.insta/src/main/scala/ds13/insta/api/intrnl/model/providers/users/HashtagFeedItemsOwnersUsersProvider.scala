package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.InstaAPIExecutor
import ds13.insta.api.intrnl.model.EmptyFeed
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId

object HashtagFeedItemsOwnersUsersProvider {

  val NAME = "HashtagFeedItemsOwnersUsersProvider"

}

class HashtagFeedItemsOwnersUsersProvider(
  override val api: InstaAPIExecutor,
  val hashtag: String)
    extends TraitHashtagFeedItemsOwnersUsersProvider {

  override val name = HashtagFeedItemsOwnersUsersProvider.NAME

  override def getNextContainer(iapi: InstaAPIExecutor, nextMaxIdOpt: Option[String]): TraitUsersContainerWithNextMaxId =
    iapi.getHashtagFeeds(hashtag, nextMaxIdOpt)

  override def getJSONState: Option[JSONObject] =
    Some(super.getJSONState.getOrElse(new JSONObject).accumulate("hashtag", hashtag))

}