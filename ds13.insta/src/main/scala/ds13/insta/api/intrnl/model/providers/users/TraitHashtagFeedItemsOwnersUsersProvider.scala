package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.intrnl.model.EmptyFeed
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId

trait TraitHashtagFeedItemsOwnersUsersProvider
    extends TraitUsersContainerWithMaxIdUsersProvider {

  override def emptyContainer: TraitUsersContainerWithNextMaxId =
    EmptyFeed()

}