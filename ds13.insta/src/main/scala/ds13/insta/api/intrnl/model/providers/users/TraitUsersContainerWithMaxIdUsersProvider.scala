package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.InstaAPIExecutor
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId
import ds13.insta.api.intrnl.model.User

trait TraitUsersContainerWithMaxIdUsersProvider extends TraitUsersProvider {

  var opt: Option[TraitUsersContainerWithNextMaxId] = None

  var index = 0

  def updateOpt[T](nextMaxIdOpt: Option[String])(f: TraitUsersContainerWithNextMaxId => T): T = {
    index = 0
    opt = Some(getNextContainer(api, nextMaxIdOpt))
    f(opt.get)
  }

  def emptyContainer: TraitUsersContainerWithNextMaxId

  override def hasNext: Boolean =
    opt.fold(updateOpt(None)(_.getSize > 0)) { container =>
      index < container.getSize ||
        container.getNextMaxId.fold(false)(nextMaxIdOpt => updateOpt(Some(nextMaxIdOpt))(_ => hasNext))
    }

  override def apply[T](f: User => T): T =
    f(opt.get.getUser {
      index += 1
      index - 1
    })

  def getNextContainer(iapi: InstaAPIExecutor, nextMaxId: Option[String]): TraitUsersContainerWithNextMaxId

  override def getJSONState: Option[JSONObject] =
    TraitUsersContainerWithMaxIdUsersProvider.this.opt.flatMap(_.toJSON.map { t =>
      new JSONObject {
        accumulate("container", t)
        accumulate("index", index)
      }
    })

}