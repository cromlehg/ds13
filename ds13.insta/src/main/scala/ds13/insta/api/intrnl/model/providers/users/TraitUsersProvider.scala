package ds13.insta.api.intrnl.model.providers.users

import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.providers.TraitProvider

trait TraitUsersProvider extends TraitProvider[User]