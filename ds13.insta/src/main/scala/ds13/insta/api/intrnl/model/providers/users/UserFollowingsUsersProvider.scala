package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.InstaAPIExecutor
import ds13.insta.api.intrnl.model.EmptyUsersList
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId

object UserFollowingsUsersProvider {

  val NAME = "UserFollowingsUsersProvider"

}

class UserFollowingsUsersProvider(
  override val api: InstaAPIExecutor,
  val username: String)
    extends TraitUsersContainerWithMaxIdUsersProvider {

  override val name = UserFollowingsUsersProvider.NAME

  var userIdOpt: Option[Long] = None

  override def getNextContainer(iapi: InstaAPIExecutor, nextMaxIdOpt: Option[String]): TraitUsersContainerWithNextMaxId = {
    val uid = userIdOpt.getOrElse {
      val localUid = api.getUsernameInfo(username).id
      userIdOpt = Some(localUid)
      localUid
    }
    api.getFollowers(uid, nextMaxIdOpt)
  }

  override def emptyContainer: TraitUsersContainerWithNextMaxId =
    EmptyUsersList()

  override def getJSONState: Option[JSONObject] =
    Some {
      val o = super.getJSONState.getOrElse(new JSONObject)
      o.accumulate("username", username)
      userIdOpt.foreach(userId => o.accumulate("user_id", userId))
      o
    }

}