package ds13.insta.api.intrnl.tests

import ds13.insta.api.intrnl.InternalAPI2
import ds13.common.PrintAllLogger

object Test1 extends App {

  val logger = PrintAllLogger()

  val api = new InternalAPI2(
    logger,
    None,
    "5.0.7",
    "Android (19/4.4.2; 160dpi; 720x1104; Samsung; GT-I9500; ja3g; unknown; en_EN)",
    "f44a81b4506c4d6f",
    "8be43338-78f8-40b1-adca-799cca6bb14e",
    "username",
    "password")

  
  val info =  api.getSelfUserInfo
  println(info)

}