package ds13.insta.api.web

class RequestNode(val name: String) {

  def toStringIndented(startIndent: String, indent: String): String =
    startIndent + name

  override def toString =
    toStringIndented("", "\t")

}

class RequestValueNode(name: String, val value: Object)
    extends RequestNode(name) {

  override def toStringIndented(startIndent: String, indent: String): String =
    super.toStringIndented(startIndent, indent) + ": " + value.toString

}

class RequestContainerNode(name: String, val childs: RequestNode*)
    extends RequestNode(name) {

  override def toStringIndented(startIndent: String, indent: String): String =
    super.toStringIndented(startIndent, indent) +
      childsToStringIndented(startIndent, indent)

  def childsToStringIndented(startIndent: String, indent: String) =
    childs.map(_.toStringIndented(startIndent + indent, indent)).mkString(" {\n", ",\n", "\n" + startIndent + "}")

}

class RequestContainerFunctionNode(name: String, values: Seq[Any], childs: RequestNode*)
    extends RequestContainerNode(name, childs: _*) {

  override def toStringIndented(startIndent: String, indent: String): String =
    startIndent + name + values.mkString("(", ", ", ")") +
      childsToStringIndented(startIndent, indent)

}

// IGUser container function

class IARIGUserFunctionRequest(userId: Long, childs: RequestNode*)
  extends RequestContainerFunctionNode("ig_user", Seq(userId), childs: _*)

// Follows functions not wrapped int IG_User

class IARFollowsFunction(followsFunctionName: String, followsFunctionValues: Seq[Any], childs: RequestNode*)
  extends RequestContainerFunctionNode("follows." + followsFunctionName, followsFunctionValues, childs: _*)

class IARFollowedByFunction(followsFunctionName: String, followsFunctionValues: Seq[Any], childs: RequestNode*)
  extends RequestContainerFunctionNode("followed_by." + followsFunctionName, followsFunctionValues, childs: _*)

class IARFollowsFunctionFirst(size: Int, childs: RequestNode*)
  extends IARFollowsFunction("first", Seq(size), childs: _*)

class IARFollowsFunctionAfter(size: Int, cursor: String, childs: RequestNode*)
  extends IARFollowsFunction("after", Seq(cursor, size), childs: _*)

class IARFollowedByFunctionFirst(size: Int, childs: RequestNode*)
  extends IARFollowedByFunction("first", Seq(size), childs: _*)

class IARFollowedByFunctionAfter(size: Int, cursor: String, childs: RequestNode*)
  extends IARFollowedByFunction("after", Seq(cursor, size), childs: _*)

// Follows functions wrapped into IG_User

class IARIGUserFollowsFirstFunctionRequest(userId: Long, size: Int, childs: RequestNode*)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowsFunctionFirst(size, childs: _*))

class IARIGUserFollowsAfterFunctionRequest(userId: Long, size: Int, cursor: String, childs: RequestNode*)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowsFunctionAfter(size, cursor, childs: _*))

class IARIGUserFollowedByFirstFunctionRequest(userId: Long, size: Int, childs: RequestNode*)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowedByFunctionFirst(size, childs: _*))

class IARIGUserFollowedByAfterFunctionRequest(userId: Long, size: Int, cursor: String, childs: RequestNode*)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowedByFunctionAfter(size, cursor, childs: _*))

// Common use functions

class IARIGUserFollowsFirstFunctionRequestDefault(userId: Long, size: Int)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowsFunctionFirst(size,
    new RequestNode("count"),
    new RequestContainerNode("page_info",
      new RequestNode("end_cursor"),
      new RequestNode("has_next_page")),
    new RequestContainerNode("nodes",
      new RequestNode("id"))))

class IARIGUserFollowsAfterFunctionRequestDefault(userId: Long, size: Int, cursor: String)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowsFunctionAfter(size, cursor,
    new RequestNode("count"),
    new RequestContainerNode("page_info",
      new RequestNode("end_cursor"),
      new RequestNode("has_next_page")),
    new RequestContainerNode("nodes",
      new RequestNode("id"))))

class IARIGUserFollowedByFirstFunctionRequestDefault(userId: Long, size: Int)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowedByFunctionFirst(size,
    new RequestNode("count"),
    new RequestContainerNode("page_info",
      new RequestNode("end_cursor"),
      new RequestNode("has_next_page")),
    new RequestContainerNode("nodes",
      new RequestNode("id"))))

class IARIGUserFollowedByAfterFunctionRequestDefault(userId: Long, size: Int, cursor: String)
  extends IARIGUserFunctionRequest(userId: Long, new IARFollowedByFunctionAfter(size, cursor,
    new RequestNode("count"),
    new RequestContainerNode("page_info",
      new RequestNode("end_cursor"),
      new RequestNode("has_next_page")),
    new RequestContainerNode("nodes",
      new RequestNode("id"))))
