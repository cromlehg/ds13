

import com.google.inject.AbstractModule

import controllers.modules.robots.RWSBotController
import javax.inject.Singleton

class Module extends AbstractModule {

  override def configure() = {

    bind(classOf[RWSBotController]).in(classOf[Singleton])

  }

}
