package controllers

import scala.concurrent.Future

import controllers.modules.robots.RWSBotController
import ds13.bots.CommmonImplicits.appContext
import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi
import play.api.libs.json.Json
import play.api.mvc.BodyParsers
import play.api.mvc.Action
import org.mindrot.jbcrypt.BCrypt
import jp.t2v.lab.play2.auth.LoginLogout
import models.Roles

@Singleton
class APIv1 @Inject() (override val dao: DAO, val bc: RWSBotController, val messagesApi: MessagesApi) extends TraitCheckOwnerResourceForUser with I18nSupport with LoginLogout {

  override val isAPIv1 = true

  def test(userId: Long) =
    ActionCheckAdminOrOwnerByUser(userId, BodyParsers.parse.json) { ownerUser =>
      implicit user => implicit request =>
        val value = (request.body \ "id").as[Long]
        Future(Ok(Json.obj("status" -> "ok")))
    }

  def updateLaunch(aid: Long) = TODO

  def playLaunch(aid: Long) = TODO

  def stopLaunch(aid: Long) = TODO

  def removeAccount(aid: Long) = TODO

  def createAccount(uid: Long) = TODO

  def listAccounts(uid: Long, pid: Long) = TODO

  def registerUser = TODO

  def verifyUser = TODO

  //curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:9000/api/v1/login -d "{\"email\":\"cromlehg@gmail.com\",\"password\":\"tenv1\"}"
  //curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:9000/api/v1/login -d "{\"email\":\"cromlehg@gmail.com\",\"password\":\"tenv12\"}"
  def login = Action.async(BodyParsers.parse.json) { implicit request =>
    dao.findUserByEmail((request.body \ "email").as[String]) flatMap {
      case Some(user) =>
        user.hash match {
          case Some(hash) =>
            if (BCrypt.checkpw((request.body \ "password").as[String], hash))
              gotoLoginSucceeded(user.id)
            else
              Future.successful(Ok(Json.obj("status" -> "failed")))
          case None => Future.successful(BadRequest)
        }
      case _ => Future.successful(BadRequest)
    }
  }

  //curl -i -H "Accept: application/json" -X POST http://localhost:9000/api/v1/logout
  def logout = AuthorizationAction(Roles.CLIENT).async {
    implicit request =>
      implicit val user = request.user
      gotoLogoutSucceeded
  }

}
