package controllers

import jp.t2v.lab.play2.auth.AsyncAuth
import jp.t2v.lab.play2.auth.LoginLogout
import jp.t2v.lab.play2.auth.AuthElement

import jp.t2v.lab.play2.auth.AuthActionBuilders

import javax.inject._
import play.api._
import play.api.mvc._

import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import play.api.data._
import play.api.data.Form
import play.api.data.Forms.email
import play.api.data.Forms.text
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText

import models.daos.DAO
import scala.util.Random

import org.mindrot.jbcrypt.BCrypt
import controllers.security.AuthConfigImpl
import play.mvc.Http.RequestHeader
import models.User
import models.Roles

@Singleton
class AccountTypesController @Inject() (override val dao: DAO) extends TraitUserServiceWithRoles {
  
  def list(pageId: Long) = AuthorizationAction(Roles.ADMIN).async { implicit request =>
    implicit val user = request.user
    dao.getAccountTypesPage(pageId).map {
      accounts => Ok(views.html.admin.subviews.accountTypesList(None, accounts))
    }
  }

  def listByUserId(userId: Long, pageId: Long) = AuthorizationAction(Roles.ADMIN).async { implicit request =>
    implicit val user = request.user
    dao.findUserById(userId) flatMap {
      case Some(ownerUser) =>
        dao.getAccountTypesPage(pageId).map {
          accounts => Ok(views.html.admin.subviews.accountTypesList(Some(ownerUser), accounts))
        }

       //dao.getAccountTypesPageByUserId(pageId, userId).map {
          //accounts => Ok(views.html.admin.subviews.accountTypesList(Some(ownerUser), accounts))
        //}
      case None => Future(BadRequest("Пользователь с таким id = " + userId + " не найден"))
    }
  }

  def profile(accountTypeId: Long) = TODO

}
