package controllers

import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi
import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import play.api.Logger
import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO
import ds13.bots.BotSystem
import controllers.modules.robots.RWSBotController
import models.Roles
import ds13.bots.storable.StorableBotLaunchHelper
import play.api.mvc.Action

@Singleton
class AccountsController @Inject() (override val dao: DAO, val bc: RWSBotController, val messagesApi: MessagesApi) extends TraitCheckOwnerResourceForUser with I18nSupport {

  def remove(accountId: Long) =
    ActionCheckAdminOrOwnerByAccount(accountId) { (account, ownerUser) =>
      implicit user => implicit request =>
        dao.getLaunchesPageByAccountId(1, accountId) flatMap { launches =>
          if (launches.isEmpty)
            dao.deleteAccount(accountId) flatMap { result =>
              if (result)
                dao.removeProxyAssignFromAccount(accountId) map { isRemoved =>
                  if (isRemoved) {
                    Redirect(request headers "referer")
                      .flashing("success" -> ("Account successfully removed!"))
                  } else {
                    Redirect(request headers "referer")
                      .flashing("success" -> ("Account successfully removed!"))
                      .flashing("error" -> ("Proxy assign not removed for account " + accountId))
                  }
                }
              else
                Future.successful(Redirect(request headers "referer")
                  .flashing("error" -> ("Error during account remove!")))
            }
          else
            Future.successful(Redirect(request headers "referer")
              .flashing("error" -> ("Could not remove account with launches!")))
        }
    }

  def listByUserIdAndAccountTypeId(userId: Long, accountTypeId: Long, pageId: Long) =
    ActionCheckAdminOrOwnerByUser(userId) { ownerUser =>
      implicit user => implicit request =>
        dao.getAccountsWithOwnersPageByUserIdAndAccountTypeId(pageId, userId, accountTypeId).map {
          accountsWithOwners => Ok(bc.idToAccountTypeViewHelper.get(accountTypeId).accountsList(Some(ownerUser), accountsWithOwners))
        }
    }

  def listByUserId(userId: Long, pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.findUserById(userId) flatMap {
        case Some(ownerUser) =>
          dao.getATUPageByUserId(pageId, userId).map {
            atu => Ok(views.html.admin.subviews.accountsList(Some(ownerUser), atu))
          }
        case None => Future(BadRequest("Пользователь с таким id = " + userId + " не найден"))
      }
    }

  def listByAccountTypeId(accountTypeId: Long, pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getATUPageByAccountTypeId(pageId, accountTypeId).map {
        atu => Ok(views.html.admin.subviews.accountsList(None, atu))
      }
    }

  def listByRobotId(robotId: Long, pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getATUPageByRobotId(pageId, robotId).flatMap {
        atu => Future(Ok(views.html.admin.subviews.accountsList(None, atu)))
      }
    }

  def list(pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getATUPage(pageId).map {
        atu => Ok(views.html.admin.subviews.accountsList(None, atu))
      }
    }

  def listSpecific(accountTypeId: Long, pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getAccountsWithOwnersPageByAccountTypeId(pageId, accountTypeId).map { accountsWithOwners =>
        Ok(bc.idToAccountTypeViewHelper.get(accountTypeId).accountsList(None, accountsWithOwners))
      }
    }

  def getLaunchHelpers(launches: Seq[ds13.bots.storable.Launch]): Seq[(ds13.bots.storable.Launch, Option[StorableBotLaunchHelper])] =
    launches.map(launch => (launch, {
      if (bc.idToLaunchHelpers.containsKey(launch.id)) {
        Some(bc.idToLaunchHelpers.get(launch.id).asInstanceOf[StorableBotLaunchHelper])
      } else {
        None
      }
    }))

  def profile(accountId: Long) = Action.async {
    dao.getAccountById(accountId) map {
      case Some(account) => Redirect(bc.idToAccountTypeViewHelper.get(account.accountTypeId).profileCall(accountId))
      case _             => BadRequest("Could't get account with id " + accountId)
    }
  }

  def newAccount(userId: Long, accountTypeId: Long) =
    ActionCheckAdminOrOwnerByUser(userId) { ownerUser =>
      implicit user => implicit request =>
        Future.successful(Redirect(bc.idToAccountTypeViewHelper.get(accountTypeId).newAccount(userId)))
    }

}
