package controllers

import javax.inject.Inject
import javax.inject.Singleton
import jp.t2v.lab.play2.auth.OptionalAuthElement
import models.daos.DAO
import play.api.mvc.Action

@Singleton
class AppController @Inject() (val dao: DAO) extends TraitUserServiceWithRoles with OptionalAuthElement {

  def options(path: String) = Action {
    Ok("").withHeaders(
      "Access-Control-Allow-Origin" -> "*",
      "Access-Control-Allow-Methods" -> "GET, POST, OPTIONS",
      "Access-Control-Allow-Headers" -> "Accept, Origin, Content-type, X-Json, X-Prototype-Version, X-Requested-With",
      "Access-Control-Allow-Credentials" -> "true",
      "Access-Control-Max-Age" -> (60 * 60 * 24).toString)
  }

}
