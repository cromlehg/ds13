package controllers

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import ds13.bots.CommmonImplicits.appContext
import models.User
import play.api.mvc.ActionBuilder
import play.api.mvc.ActionRefiner
import play.api.mvc.AnyContent
import play.api.mvc.Request
import play.api.mvc.Result
import play.api.mvc.WrappedRequest
import play.api.mvc.BodyParser

class OwnerRequest[A](val owner: User, val request: Request[A]) extends WrappedRequest[A](request)

class AccountRequest[A](val account: ds13.bots.storable.Account, val request: Request[A]) extends WrappedRequest[A](request)

class LaunchRequest[A](val launch: ds13.bots.storable.Launch, val request: Request[A]) extends WrappedRequest[A](request)

class AccountOwnerRequest[A](val account: ds13.bots.storable.Account, val owner: User, val request: AccountRequest[A]) extends WrappedRequest[A](request)

class LaunchOwnerRequest[A](val launch: ds13.bots.storable.Launch, val owner: User, val request: LaunchRequest[A]) extends WrappedRequest[A](request)

trait TraitCheckOwnerResourceForUser extends TraitUserService {

  def OwnerRequestAction(id: Long) = new ActionBuilder[OwnerRequest] with ActionRefiner[Request, OwnerRequest] {

    override def refine[A](input: Request[A]) =
      dao.findUserById(id).map(_.map(t => new OwnerRequest(t, input)).toRight(NotFound))

  }

  def AccountRequestAction(id: Long) = new ActionBuilder[AccountRequest] with ActionRefiner[Request, AccountRequest] {

    override def refine[A](input: Request[A]) =
      dao.getAccountById(id).map(_.map(t => new AccountRequest(t, input)).toRight(NotFound))

  }

  def LaunchRequestAction(id: Long) = new ActionBuilder[LaunchRequest] with ActionRefiner[Request, LaunchRequest] {

    override def refine[A](input: Request[A]) =
      dao.getLaunchById(id).map(_.map(t => new LaunchRequest(t, input)).toRight(NotFound))

  }

  def AccountOwnerRequestAction = new ActionRefiner[AccountRequest, AccountOwnerRequest] {

    override def refine[A](input: AccountRequest[A]) =
      dao.findUserById(input.account.ownerId).map(_.map(t => new AccountOwnerRequest(input.account, t, input)).toRight(NotFound))

  }

  def LaunchOwnerRequestAction = new ActionRefiner[LaunchRequest, LaunchOwnerRequest] {

    override def refine[A](input: LaunchRequest[A]) =
      dao.getUserByLaunchId(input.launch.id).map(_.map(t => new LaunchOwnerRequest(input.launch, t, input)).toRight(NotFound))

  }

  def ActionCheckAdminOrOwnerByUser(id: Long)(f: User => (User => (AuthRequest[AnyContent] => Future[Result])))(implicit context: ExecutionContext) =
    OwnerRequestAction(id).async { r =>
      checkAdminOrOwner(r.owner.id)(f(r.owner)).apply(r)
    }

  def ActionCheckAdminOrOwnerByAccount(id: Long)(f: (ds13.bots.storable.Account, User) => (User => (AuthRequest[AnyContent] => Future[Result])))(implicit context: ExecutionContext) =
    (AccountRequestAction(id) andThen AccountOwnerRequestAction).async { r =>
      checkAdminOrOwner(r.owner.id)(f(r.account, r.owner)).apply(r)
    }

  def ActionCheckAdminOrOwnerByLaunch(id: Long)(f: (ds13.bots.storable.Launch, User) => (User => (AuthRequest[AnyContent] => Future[Result])))(implicit context: ExecutionContext) =
    (LaunchRequestAction(id) andThen LaunchOwnerRequestAction).async { r =>
      checkAdminOrOwner(r.owner.id)(f(r.launch, r.owner)).apply(r)
    }

  def ActionCheckAdminOrOwnerByUser[A](id: Long, p: BodyParser[A])(f: User => (User => (AuthRequest[A] => Future[Result])))(implicit context: ExecutionContext) =
    OwnerRequestAction(id).async(p) { r =>
      checkAdminOrOwner(r.owner.id, p)(f(r.owner)).apply(r.request)
    }

  def ActionCheckAdminOrOwnerByAccount[A](id: Long, p: BodyParser[A])(f: (ds13.bots.storable.Account, User) => (User => (AuthRequest[A] => Future[Result])))(implicit context: ExecutionContext) =
    (AccountRequestAction(id) andThen AccountOwnerRequestAction).async(p) { r =>
      checkAdminOrOwner(r.owner.id, p)(f(r.account, r.owner)).apply(r.request.request)
    }

  def ActionCheckAdminOrOwnerByLaunch[A](id: Long, p: BodyParser[A])(f: (ds13.bots.storable.Launch, User) => (User => (AuthRequest[A] => Future[Result])))(implicit context: ExecutionContext) =
    (LaunchRequestAction(id) andThen LaunchOwnerRequestAction).async(p) { r =>
      checkAdminOrOwner(r.owner.id, p)(f(r.launch, r.owner)).apply(r.request.request)
    }

}
