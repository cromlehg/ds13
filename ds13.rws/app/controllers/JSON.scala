package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import models.daos.DAO
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

class JSONResponse(val status: String, val descr: String = "")

object JSONResponse {

  val STATUS_OK = "ok"

  val STATUS_ERROR = "error"

  def apply(status: String, descr: String) =
    new JSONResponse(status, descr)

  implicit val writes = new Writes[JSONResponse] {
    def writes(r: JSONResponse) = Json.obj(
      "status" -> r.status,
      "descr" -> r.descr)
  }

}

object JSONResponseOk {

  def apply(): JsValue =
    Json.toJson(new JSONResponse(JSONResponse.STATUS_OK))

  def apply(descr: String): JsValue =
    Json.toJson(new JSONResponse(JSONResponse.STATUS_OK, descr))

}

object JSONResponseError {

  def apply(descr: String): JsValue =
    Json.toJson(new JSONResponse(JSONResponse.STATUS_ERROR, descr))

}
