package controllers

import scala.concurrent.Future

import controllers.modules.robots.RWSBotController
import ds13.bots.BotConfig
import ds13.bots.CommmonImplicits.appContext
import ds13.bots.storable.StorableBotLaunchHelper
import ds13.bots.storable.StorableBots
import javax.inject.Inject
import javax.inject.Singleton
import models.Roles
import models.daos.DAO
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi
import play.api.mvc.Flash
import controllers.modules.robots.LogStorageLogger

@Singleton
class LaunchesController @Inject() (override val dao: DAO, val bc: RWSBotController, val messagesApi: MessagesApi)
    extends TraitCheckOwnerResourceForUser with I18nSupport {

  def launchAction(launchId: Long, command: String) =
    ActionCheckAdminOrOwnerByLaunch(launchId) { (launch, ownerUser) =>
      implicit user => implicit request =>
        Future.successful {
          if (bc.idToLaunchHelpers.containsKey(launchId)) {
            bc.idToLaunchHelpers.get(launchId).sendMsg(command)
            Redirect(request headers "referer")
              .flashing("success" -> ("Action successfully sended to robot!"))
            //Redirect(controllers.routes.AccountsController.profile(launch.accountId))
          } else
            BadRequest("Can't take launch with id " + launchId + " in controller")
        }
    }

  //  def forceRemove(launchId: Long) =
  //    ActionCheckAdminOrOwnerByLaunch(launchId) { (launch, ownerUser) =>
  //      implicit user => implicit request =>
  //        Future.successful {
  //          if (bc.idToLaunchHelpers.containsKey(launchId)) {
  //            val launchHelper = bc.idToLaunchHelpers.containsKey(launchId)
  //            launchHelper.
  //            //bc.idToLaunchHelpers.get(launchId).sendMsg(command)
  //            Redirect(controllers.routes.AccountsController.profile(launch.accountId))
  //          } else
  //            BadRequest("Can't take launch with id " + launchId + " in controller")
  //        }
  //    }

  def readLog(launchId: Long) =
    ActionCheckAdminOrOwnerByLaunch(launchId) { (launch, ownerUser) =>
      implicit user => implicit request =>
        if (bc.idToLaunchHelpers.containsKey(launchId)) {
          Future.successful(Ok(
            views.html.admin.subviews.robotLog(
              bc.idToLaunchHelpers.get(launchId).botProcessLogger.asInstanceOf[LogStorageLogger].getContent(100))))
        } else
          Future.successful(BadRequest("Can't take launch with id " + launchId + " in read log"))
    }

  def createLaunch(accountId: Long, robotId: Long) =
    ActionCheckAdminOrOwnerByAccount(accountId) { (account, ownerUser) =>
      implicit user => implicit request =>
        dao.getRobotById(robotId) map {
          case Some(robot) =>
            if (robot.accountTypeId == account.accountTypeId) {
              val descr = bc.idToDescriptors.get(robotId)
              if (descr == null)
                BadRequest("Could not get descriptor for " + robotId)
              else {
                bc.idToRobotViewHelper.get(robotId).configForm.bindFromRequest.fold(
                  hasErrors = { form =>
                    Redirect(controllers.routes.LaunchesController.configureNewLaunch(accountId, robotId)).
                      flashing(Flash(form.data) + ("error" -> "Проблемы валидации формы"))
                  },
                  success = { data =>
                    val config = new BotConfig(StorableBots.ACCOUNT_ID -> accountId)
                    bc.idToRobotViewHelper.get(robotId).updateConfig(data, config)
                    bc.idToDescriptors.get(robotId).createBotLaunchHelper.prepareProcess(config)
                    Redirect(controllers.routes.AccountsController.profile(accountId))
                  })
              }
            } else
              BadRequest("Account for " + accountId + " and robot " + robotId + " types must be equals!")
          case _ => BadRequest("Could no get robot with id: " + robotId)
        }
    }

  def configureNewLaunch(accountId: Long, robotId: Long) =
    ActionCheckAdminOrOwnerByAccount(accountId) { (account, ownerUser) =>
      implicit user => implicit request =>
        dao.getRobotById(robotId) map {
          case Some(robot) =>
            if (robot.accountTypeId == account.accountTypeId) {
              val descr = bc.idToDescriptors.get(robotId)
              if (descr == null)
                BadRequest("Could not get descriptor for " + robotId)
              else
                Ok(bc.idToRobotViewHelper.get(robotId).configPage(ownerUser, accountId))
            } else
              BadRequest("Account for " + accountId + " and robot " + robotId + " types must be equals!")
          case _ => BadRequest("Could no get robot with id: " + robotId)
        }
    }

  def getLaunchHelpers(launches: Seq[(ds13.bots.storable.Launch, models.daos.DBUser, ds13.bots.storable.AccountType, ds13.bots.storable.Account)]): Seq[(ds13.bots.storable.Launch, models.daos.DBUser, ds13.bots.storable.AccountType, ds13.bots.storable.Account, Option[StorableBotLaunchHelper])] =
    launches.map {
      case (launch, user, atype, account) => (launch, user, atype, account, {
        if (bc.idToLaunchHelpers.containsKey(launch.id)) {
          Some(bc.idToLaunchHelpers.get(launch.id).asInstanceOf[StorableBotLaunchHelper])
        } else {
          None
        }
      })
    }

  def listByUserIdAndAccountTypeId(userId: Long, accountTypeId: Long, pageId: Long) =
    ActionCheckAdminOrOwnerByUser(userId) { ownerUser =>
      implicit user => implicit request =>
        dao.getLaunchesGroupPageByUserIdAndAccountTypeId(pageId, accountTypeId, userId).map {
          launches => Ok(views.html.admin.subviews.launchesList(Some(ownerUser), getLaunchHelpers(launches)))
        }
    }

  def listByAccountId(accountId: Long, pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getAccountById(accountId) flatMap {
        case Some(account) =>
          dao.findUserById(account.ownerId) flatMap {
            case Some(ownerUser) =>
              dao.getLaunchesGroupedPageByAccountId(pageId, accountId).map {
                launches =>
                  Ok(views.html.admin.subviews.launchesList(Some(ownerUser), getLaunchHelpers(launches)))
              }
            case None => Future(BadRequest("Пользователь с таким id = " + account.ownerId + " не найден"))
          }
        case None => Future(BadRequest("Аккаунт с таким id = " + accountId + " не найден"))
      }
    }

  def listByUserId(userId: Long, pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.findUserById(userId) flatMap {
        case Some(ownerUser) =>
          dao.getLaunchesGroupedPageByUserId(pageId, userId).map {
            launches =>
              Ok(views.html.admin.subviews.launchesList(Some(ownerUser), getLaunchHelpers(launches)))
          }
        case None => Future(BadRequest("Пользователь с таким id = " + userId + " не найден"))
      }
    }

  def list(pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getLaunchesGroupedPage(pageId).map {
        launches => Ok(views.html.admin.subviews.launchesList(None, getLaunchHelpers(launches)))
      }
    }

  def profile(launchId: Long) =
    ActionCheckAdminOrOwnerByLaunch(launchId) { (launch, ownerUser) =>
      implicit user => implicit request =>
        Future(if (bc.idToLaunchHelpers.containsKey(launchId))
          Ok(bc.idToRobotViewHelper.get(launch.robotId).statePage(ownerUser, launch, bc.idToLaunchHelpers.get(launchId)))
        else
          BadRequest("Can't take launch with id " + launchId + " in controller"))
    }

}
