package controllers

import scala.concurrent.Future
import scala.util.Random

import org.mindrot.jbcrypt.BCrypt

import ds13.bots.CommmonImplicits.appContext
import javax.inject.Inject
import javax.inject.Singleton
import jp.t2v.lab.play2.auth.LoginLogout
import models.Roles
import models.daos.DAO
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms.email
import play.api.data.Forms.mapping
import play.api.data.Forms.text
import play.api.mvc.Action

@Singleton
class PanelController @Inject() (override val dao: DAO) extends TraitUserService with LoginLogout {

  case class AuthData(val email: String, val pass: String)

  val random = Random

  val authForm = Form(
    mapping(
      "email" -> email,
      "pass" -> text)(AuthData.apply)(AuthData.unapply))

  def login = Action.async {
    Future {
      Logger.debug("login called")
      Ok(views.html.admin.security.login(authForm))
    }
  }

  def panel =
    AuthorizationAction(Roles.CLIENT) { implicit request =>
      implicit val user = request.user
      Logger.debug("panel called")
      if (user.isAdmin) {
        Ok(views.html.admin.index())
      } else
        Redirect(controllers.routes.AccountsController.listByUserIdAndAccountTypeId(user.id, 1, 1))
    }

  def logout = AuthorizationAction(Roles.CLIENT).async {
    implicit request =>
      implicit val user = request.user
      gotoLogoutSucceeded
  }

  def auth = Action.async { implicit request =>
    Logger.debug("auth()")
    authForm.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(views.html.admin.security.login(formWithErrors))), { authData =>
        dao.findUserByEmail(authData.email) flatMap {
          case Some(user) =>
            user.hash match {
              case Some(hash) =>
                if (BCrypt.checkpw(authData.pass, hash))
                  gotoLoginSucceeded(user.id)
                else
                  Future(BadRequest(views.html.admin.security.login(authForm)))
              case None => Future(BadRequest(views.html.admin.security.login(authForm)))
            }
          case None => Future(BadRequest(views.html.admin.security.login(authForm)))
        }
      })
  }

}

