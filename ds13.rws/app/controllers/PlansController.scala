package controllers

import org.json.JSONObject

import controllers.modules.robots.RWSBotController
import ds13.bots.CommmonImplicits.appContext
import ds13.common.TraitJSON
import javax.inject.Inject
import javax.inject.Singleton
import models.Roles
import models.daos.DAO
import play.api.data.Form
import play.api.data.Forms.longNumber
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi

object PlanConfConsts {

  val FIELD_NAME = "name"

  val FIELD_PRICE = "price"

}

case class PlanConf(
    val name: String,
    val price: Long) extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate(PlanConfConsts.FIELD_NAME, name)
      accumulate(PlanConfConsts.FIELD_PRICE, price)
    })

}

@Singleton
class PlansController @Inject() (override val dao: DAO, val bc: RWSBotController, val messagesApi: MessagesApi) extends TraitCheckOwnerResourceForUser with I18nSupport {

  def configNewForm: Form[PlanConf] = Form(
    mapping(
      PlanConfConsts.FIELD_NAME -> nonEmptyText,
      PlanConfConsts.FIELD_PRICE -> longNumber)(PlanConf.apply)(PlanConf.unapply))

  def remove(accountId: Long) = TODO

  def list(pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getPlansPage(pageId).map {
        plans => Ok(views.html.admin.subviews.plansList(plans))
      }
    }

  def configureNew() = TODO

  def createNew() = TODO

  def profile(id: Long) = TODO

}
