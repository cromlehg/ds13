package controllers

import scala.concurrent.Future

import org.json.JSONObject

import controllers.modules.robots.RWSBotController
import ds13.bots.CommmonImplicits.appContext
import ds13.common.TraitJSON
import javax.inject.Inject
import javax.inject.Singleton
import models.Roles
import models.daos.DAO
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.number
import play.api.data.Forms.optional
import play.api.data.Forms.text
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi
import play.api.mvc.Flash

object ProxyConfConsts {

  val FIELD_IP = "ip"

  val FIELD_PORT = "port"

  val FIELD_AUTH_TYPE = "authtype"

  val FIELD_LOGIN = "login"

  val FIELD_PASSWORD = "password"

}

case class ProxyConf(
    val ip: String,
    val port: Option[Int],
    val authtype: Int,
    val login: Option[String],
    val password: Option[String]) extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate(ProxyConfConsts.FIELD_IP, ip)
      port.foreach(t => accumulate(ProxyConfConsts.FIELD_PORT, t))
      accumulate(ProxyConfConsts.FIELD_AUTH_TYPE, authtype)
      login.foreach(t => accumulate(ProxyConfConsts.FIELD_LOGIN, t))
      password.foreach(t => accumulate(ProxyConfConsts.FIELD_PASSWORD, t))
    })

}

@Singleton
class ProxiesController @Inject() (override val dao: DAO, val bc: RWSBotController, val messagesApi: MessagesApi) extends TraitUserServiceWithRoles with I18nSupport {

  val ipRegex = """(25[0-5]|2[0-4][0-9]|[1][0-9][0-9]|[1-9][0-9]|[0-9]?)(\.(25[0-5]|2[0-4][0-9]|[1][0-9][0-9]|[1-9][0-9]|[0-9]?)){3}""".r

  val ipPattern = ipRegex.pattern

  def configNewForm: Form[ProxyConf] = Form(
    mapping(
      ProxyConfConsts.FIELD_IP -> nonEmptyText,
      ProxyConfConsts.FIELD_PORT -> optional(number),
      ProxyConfConsts.FIELD_AUTH_TYPE -> number,
      ProxyConfConsts.FIELD_LOGIN -> optional(text),
      ProxyConfConsts.FIELD_PASSWORD -> optional(text))(ProxyConf.apply)(ProxyConf.unapply))

  def list(pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getProxiesPageFullInfo(pageId).map {
        proxies => Ok(views.html.admin.subviews.proxiesList(proxies))
      }
    }

  def remove(proxyId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getLaunchesAssignedWithProxy(proxyId) flatMap { proxyLaunchesAssigns =>
        if (proxyLaunchesAssigns.nonEmpty)
          Future.successful(Redirect(request headers "referer")
            .flashing("error" -> ("Proxy " + proxyId + " assigned to launches: " +
              proxyLaunchesAssigns.map(_.launchId).mkString(", ")
              + ". Please remove launches before.")))
        else
          dao.removeProxyWithAssigns(proxyId) map { isRemoved =>
            if (isRemoved)
              // FIXME: Can redirect to removed page !
              Redirect(request headers "referer")
                .flashing("success" -> ("Proxy with ID " + proxyId + " successfully removed!"))
            else
              Redirect(request headers "referer")
                .flashing("error" -> ("Couldn't remove proxy with ID " + proxyId + "."))
          }
      }
    }

  def createProxy() =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      configNewForm.bindFromRequest.fold(
        hasErrors = { form =>
          Future.successful(Ok(views.html.admin.subviews.addProxy(form)).
            flashing(Flash(form.data) + ("error" -> "Проблемы валидации формы")))
        },
        success = { data =>
          if (ipPattern.matcher(data.ip).matches) {
            val splittedIp = data.ip.split("\\.")
            if (splittedIp(0).length() < 4 && splittedIp(0).length() > 0 &&
              splittedIp(1).length() < 4 && splittedIp(1).length() > 0 &&
              splittedIp(2).length() < 4 && splittedIp(2).length() > 0 &&
              splittedIp(3).length() < 4 && splittedIp(3).length() > 0) {
              Future(Ok(""))
              dao.isExistsProxyByIP(data.ip.trim) flatMap { isExists =>
                if (isExists) {
                  val form = configNewForm.fill(data)
                  Future.successful(Ok(views.html.admin.subviews.addProxy(form)).
                    flashing(Flash(form.data) + ("error" -> "Прокси с таким IP уже существует!")))
                } else
                  dao.createProxy(data.ip, data.port, 1, data.login, data.password) map (_ match {
                    case Some(proxy) =>
                      Redirect(controllers.routes.ProxiesController.list(1)).
                        flashing("success" -> ("Прокси " + proxy.ip + " успешно создан!"))
                    case _ =>
                      val form = configNewForm.fill(data)
                      Ok(views.html.admin.subviews.addProxy(form)).
                        flashing(Flash(form.data) + ("error" -> "Проблемы при создании прокси!"))
                  })
              }
            } else {
              val form = configNewForm.fill(data)
              Future.successful(Ok(views.html.admin.subviews.addProxy(form)).
                flashing(Flash(form.data) + ("error" -> "Проблемы валидации формы - некоррекный IP")))
            }
          } else {
            val form = configNewForm.fill(data)
            Future.successful(Ok(views.html.admin.subviews.addProxy(form)).
              flashing(Flash(form.data) + ("error" -> "Проблемы валидации формы - некоррекный IP")))
          }
        })
    }

  def configureNew() =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      val form = configNewForm.fill(ProxyConf("", Some(80), 1, None, None))
      Future.successful(Ok(views.html.admin.subviews.addProxy(form)))
    }

  def profile(robotId: Long) = TODO

}
