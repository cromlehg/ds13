package controllers

import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO
import ds13.bots.BotSystem
import controllers.modules.robots.RWSBotController
import models.Roles

@Singleton
class RobotsController @Inject() (override val dao: DAO, val bc: RWSBotController) extends TraitUserServiceWithRoles {

  def list(pageId: Long) =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getRobotsPage(pageId).map {
        robots => Ok(views.html.admin.subviews.robotsList(None, robots))
      }
    }

  def profile(robotId: Long) = TODO

}
