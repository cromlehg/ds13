package controllers

import jp.t2v.lab.play2.auth.AsyncAuth
import jp.t2v.lab.play2.auth.LoginLogout
import jp.t2v.lab.play2.auth.AuthElement

import jp.t2v.lab.play2.auth.AuthActionBuilders

import javax.inject._
import play.api._
import play.api.mvc._

import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import play.api.data._
import play.api.data.Form
import play.api.data.Forms.email
import play.api.data.Forms.text
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText

import models.daos.DAO
import scala.util.Random

import org.mindrot.jbcrypt.BCrypt
import controllers.security.AuthConfigImpl
import play.mvc.Http.RequestHeader
import models.User
import models.Roles
import scala.concurrent.ExecutionContext

@Singleton
class TestController @Inject() (override val dao: DAO) extends TraitCheckOwnerResourceForUser {

  def test(userId: Long) =
    ActionCheckAdminOrOwnerByUser(userId) { owner =>
      user => request =>
        Future(Ok("Owner: " + owner.name + ", initiator: " + user.name))
    }

}
