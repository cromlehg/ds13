package controllers

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import controllers.security.AuthConfigImpl
import ds13.bots.CommmonImplicits.appContext
import jp.t2v.lab.play2.auth.AuthActionBuilders
import models.Roles
import models.daos.DAO
import play.api.mvc.Action
import play.api.mvc.AnyContent
import play.api.mvc.BodyParser
import play.api.mvc.BodyParsers
import play.api.mvc.Controller
import play.api.mvc.Result

trait TraitUserService extends Controller with AuthActionBuilders with AuthConfigImpl {

  val dao: DAO

  def checkAdminOrOwner(ownerId: Long)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext) =
    AuthorizationAction(Roles.CLIENT).async { request =>
      if (request.user.roles.contains(Roles.ADMIN) || request.user.id == ownerId)
        f.apply(request.user)(request) else Future(Forbidden("You have not access to this resource"))
    }

  def checkAdminOrOwner[A](ownerId: Long, p: BodyParser[A])(f: User => (AuthRequest[A] => Future[Result]))(implicit context: ExecutionContext) =
    AuthorizationAction(Roles.CLIENT).async(p) { request =>
      if (request.user.roles.contains(Roles.ADMIN) || request.user.id == ownerId)
        f.apply(request.user)(request) else Future(Forbidden("You have not access to this resource"))
    }

  def ownerActionAsyncRequestUser(authority: Authority, resOwnerId: Long)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext) =
    AuthorizationAction(authority).async { request =>
      if (request.user.id == resOwnerId) f.apply(request.user)(request) else Future(Forbidden("You have not access to this resource"))
    }

  def actionAsyncRequestUser(authority: Authority)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext): Action[AnyContent] =
    actionAsyncRequestUser(BodyParsers.parse.anyContent, authority)(f)

  def actionAsyncRequestUser[A](p: BodyParser[A], authority: Authority)(f: User => (AuthRequest[A] => Future[Result]))(implicit context: ExecutionContext) =
    AuthorizationAction(authority).async(p) { r => f.apply(r.user)(r) }

}

trait TraitUserServiceWithRoles extends TraitUserService {

  def ownerAdminActionAsyncRequestUser(resOwnerId: Long)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext) =
    ownerActionAsyncRequestUser(Roles.ADMIN, resOwnerId)(f)(context)

  def ownerClientActionAsyncRequestUser(resOwnerId: Long)(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext) =
    ownerActionAsyncRequestUser(Roles.CLIENT, resOwnerId)(f)(context)

  def actionAdminAsyncRequestUser(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext): Action[AnyContent] =
    actionAsyncRequestUser(Roles.ADMIN)(f)(context)

  def actionClientAsyncRequestUser(f: User => (AuthRequest[AnyContent] => Future[Result]))(implicit context: ExecutionContext): Action[AnyContent] =
    actionAsyncRequestUser(Roles.CLIENT)(f)(context)

  def actionClientAsyncRequestUser[A](p: BodyParser[A])(f: User => (AuthRequest[A] => Future[Result]))(implicit context: ExecutionContext) =
    actionAsyncRequestUser(p, Roles.CLIENT)(f)(context)
}
