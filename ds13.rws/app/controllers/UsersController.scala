package controllers

import scala.concurrent.Future

import ds13.bots.CommmonImplicits.appContext
import javax.inject.Inject
import javax.inject.Singleton
import models.Roles
import models.daos.DAO

import play.api.data.Form
import play.api.data.Forms.boolean
import play.api.data.Forms.longNumber
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.number
import play.api.data.Forms.optional
import play.api.data.Forms.text
import play.api.data._
import play.api.data.Forms._
import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO
import ds13.bots.BotSystem
import controllers.modules.robots.RWSBotController
import models.Roles
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi
import play.api.mvc.Flash
import java.util.UUID
import ds13.common.TraitJSON
import org.json.JSONObject
import ds13.bots.storable.StorableBotLaunchHelper

object UserConfConsts {
  val FIELD_LOGIN = "login"
  val FIELD_EMAIL = "email"
  val FIELD_PASSWORD = "password"
  val FIELD_NAME = "name"
  val FIELD_SURNAME = "surname"
  val FIELD_IS_ADMIN = "isAdmin"
  val FIELD_BALANCE = "balance"
  val FIELD_PLAN = "plan"
}

case class UserConf(
  val login: String,
  val email: String,
  val password: String,
  val name: Option[String],
  val surname: Option[String],
  val isAdmin: Boolean,
  val balance: Long,
  val plan: Long)

@Singleton
class UsersController @Inject() (override val dao: DAO, val messagesApi: MessagesApi)
    extends TraitCheckOwnerResourceForUser with I18nSupport {

  def configForm: Form[UserConf] = Form(
    mapping(
      UserConfConsts.FIELD_LOGIN -> nonEmptyText(minLength = 5, maxLength = 20),
      UserConfConsts.FIELD_EMAIL -> email,
      UserConfConsts.FIELD_PASSWORD -> nonEmptyText(minLength = 5, maxLength = 20),
      UserConfConsts.FIELD_NAME -> optional(text),
      UserConfConsts.FIELD_SURNAME -> optional(text),
      UserConfConsts.FIELD_IS_ADMIN -> boolean,
      UserConfConsts.FIELD_BALANCE -> longNumber,
      UserConfConsts.FIELD_PLAN -> longNumber)(UserConf.apply)(UserConf.unapply))

  def configureNewUser =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user
      dao.getAllPlans flatMap { plans =>
        Future(Ok(views.html.admin.users.addUser(configForm, plans)))
      }
    }

  def createUser =
    AuthorizationAction(Roles.ADMIN).async { implicit request =>
      implicit val user = request.user

      def redirectWithError(msg: String, form: Form[UserConf]) =
        dao.getAllPlans map { plans =>
          Ok(views.html.admin.users.addUser(form, plans)).
            flashing(Flash(form.data) + ("error" -> msg))
        }

      configForm.bindFromRequest.fold(
        hasErrors = (form => redirectWithError("Проблемы валидации формы", form)),
        success = { data =>
          dao.isLoginExists(data.login) flatMap { isLoginExists =>
            if (isLoginExists)
              redirectWithError("Пользователь с таким логином уже существует!", configForm.fill(data))
            else {
              dao.isEmailExists(data.login) flatMap { isEmailExists =>
                if (isLoginExists)
                  redirectWithError("Пользователь с таким email уже существует!", configForm.fill(data))
                else {
                  dao.getPlanById(data.plan) flatMap (_ match {
                    case Some(plan) =>
                      dao.createUser(
                        data.login,
                        data.email,
                        data.password,
                        data.name,
                        data.surname,
                        data.isAdmin,
                        data.balance,
                        data.plan,
                        plan.period.map(System.currentTimeMillis() + _ * 86400000),
                        None,
                        None,
                        None,
                        None) flatMap { createdUserOpt =>
                          createdUserOpt match {
                            case Some(createdUser) =>
                              Future.successful(Redirect(controllers.routes.UsersController.profile(createdUser.id))
                                .flashing("success" -> ("Пользователь  " + createdUser.login + " был успешно создан!")))
                            case _ =>
                              redirectWithError("Проблемы при создании пользователя!", configForm.fill(data))
                          }
                        }
                    case _ =>
                      redirectWithError("Не верный тарифный план!", configForm.fill(data))
                  })
                }
              }
            }
          }
        })
    }

  def remove(id: Long) = AuthorizationAction(Roles.ADMIN).async { implicit request =>
    implicit val user = request.user
    dao.findUserById(id) flatMap {
      case Some(userToDel) =>
        dao.getAccountsPageByUserId(1, userToDel.id) flatMap { accounts =>
          if (accounts.isEmpty)
            dao.removeUserWithRoles(id) map { t =>
              Redirect(request headers "referer")
                .flashing("success" -> ("Пользователь " + userToDel.login + " был успешно удален!"))
            }
          else
            Future.successful(Redirect(request headers "referer")
              .flashing("error" -> ("Невозможно удалить пользователя, пока у него есть аккаунты. Удалите сначала аккаунты!")))
        }
      case _ => Future.successful(Redirect(request headers "referer")
        .flashing("error" -> ("Нет такого пользователя!")))
    }
  }
  
  def update(userId: Long) = TODO

  def edit(userId: Long) =
    ActionCheckAdminOrOwnerByUser(userId) { ownerUser =>
      implicit user => implicit request =>
        Future.successful(Ok(""))
    }

  def list(pageId: Long) = AuthorizationAction(Roles.ADMIN).async { implicit request =>
    implicit val user = request.user
    dao.getUsersPage(pageId).flatMap {
      users => Future(Ok(views.html.admin.subviews.usersList(users)))
    }
  }

  def listByRobotId(robotId: Long, pageId: Long) = AuthorizationAction(Roles.ADMIN).async { implicit request =>
    implicit val user = request.user
    dao.getUsersPageByRobotId(pageId, robotId).flatMap {
      users => Future(Ok(views.html.admin.subviews.usersList(users)))
    }
  }

  def profile(userId: Long) = AuthorizationAction(Roles.ADMIN).async { implicit request =>
    implicit val user = request.user
    dao.findUserById(userId) map {
      case Some(viewedUser) => Ok(views.html.admin.users.profile.profile(viewedUser))
      case None             => BadRequest("Пользователь с таким id = " + userId + " не найден")
    }
  }

}
