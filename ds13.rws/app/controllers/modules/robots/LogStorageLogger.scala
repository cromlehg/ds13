package controllers.modules.robots

import java.io.File
import java.io.FileNotFoundException
import java.io.FileWriter
import java.io.IOException

import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import java.util.concurrent.atomic.AtomicBoolean
import scala.io.Source

class LogStorageLogger(val stoargePath: String) extends TraitBotProcessLogger {

  var launchIdOpt: Option[Long] = None

  var logLinesCount = 0

  val logLinesLimit = 10000

  var fileWriterOpt: Option[FileWriter] = None

  var index = 0

  var isLaunchCanRead = new AtomicBoolean(false)

  def logPath = stoargePath + "/" + launchIdOpt.get

  override def log(text: String, severity: Int) =
    if (launchIdOpt.isDefined) {
      if (fileWriterOpt.isEmpty) createFileWriter
      val fileWriter = rebase
      fileWriter.append(text + "\n")
      fileWriter.flush
      logLinesCount += 1
    } else play.api.Logger.debug(text)

  def getContent(size: Int): Option[String] =
    if (isLaunchCanRead.get) {
      val path = logPath
      if (new File(path).exists) Some(Source.fromFile(path, "UTF-8").getLines.toSeq.takeRight(size).mkString("\n")) else None
    } else None

  override def clean =
    launchIdOpt foreach { launchId =>
      try {
        new File(stoargePath).listFiles.filter(t => t.exists && t.isFile && t.getName.startsWith(launchId + "-old-")).foreach(_.delete)
        val file = new File(logPath)
        if (file.exists && file.isFile)
          file.delete
        val fileState = new File(logPath + "-state")
        if (fileState.exists && fileState.isFile)
          fileState.delete
      } catch {
        case e: Throwable =>
          play.Logger.error("Somthing went wrong during log storage removeing process fir launchId " + launchId, e)
      }
    }

  override def update(config: TraitBotConfig) = {
    launchIdOpt = Some(config.id)
    isLaunchCanRead.set(true)
  }

  private def createFileWriter =
    fileWriterOpt =
      try {
        val file = new File(logPath)
        if (!file.exists)
          file.createNewFile
        val fileWriter = new FileWriter(file)
        Some(fileWriter)
      } catch {
        case e: IOException =>
          play.Logger.error("Couldn't create file writer: Couldn't get path for " + logPath, e)
          None
        case e: FileNotFoundException =>
          play.Logger.error("Couldn't create file writer: Couldn't get path for " + logPath + " - file not found exception", e)
          None
      }

  private def rebase: FileWriter = {
    if (logLinesCount >= logLinesLimit) {
      fileWriterOpt foreach { fileWriter =>
        fileWriter.flush
        fileWriter.close
        new File(logPath).renameTo(new File(logPath + "-old-" + index))
        index += 1
        createFileWriter
      }
      logLinesCount = 0
    }
    fileWriterOpt.get
  }

}