package controllers.modules.robots

import play.api.Logger

object PlayLoggerWrapper {

  def apply() = new PlayLoggerWrapper()

}

class PlayLoggerWrapper() extends ds13.common.Logger {

  override def log(text: String, severity: Int) =
    severity match {
      case ds13.common.Logger.SEVERITY_INFO => Logger.info(text)
      case ds13.common.Logger.SEVERITY_ERR => Logger.error(text)
      case ds13.common.Logger.SEVERITY_WARN => Logger.warn(text)
      case _ => Logger.debug(text)
    }

}

