package controllers.modules.robots

import ds13.bots.BotSystem

import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import ds13.bots.storable.ConfigProvider
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver
import controllers.modules.robots.insta.InitializeComplexBot1
import javax.inject.Singleton

class RWSBotController @Inject() (protected val dbConfigProvider: DatabaseConfigProvider, val robotsProvider: RobotsProvider) extends BotSystem("RWS") {

  val idToAccountTypeViewHelper = new java.util.concurrent.ConcurrentHashMap[Long, TraitAccountTypeViewHelper]()

  val idToRobotViewHelper = new java.util.concurrent.ConcurrentHashMap[Long, TraitRobotViewHelper]()

  def addAccountTypeViewHelper(descr: TraitAccountTypeViewHelper): Unit =
    synchronized {
      idToAccountTypeViewHelper.put(descr.id, descr)
    }

  def addRobotViewHelper(descr: TraitRobotViewHelper): Unit =
    synchronized {
      idToRobotViewHelper.put(descr.id, descr)
    }

  val dbConfig = dbConfigProvider.get[MySQLDriver]

  robotsProvider.provide(this, dbConfig, dbConfig.db, new ConfigProvider)

}