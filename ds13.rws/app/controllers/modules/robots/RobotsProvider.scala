package controllers.modules.robots

import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import ds13.bots.storable.ConfigProvider
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver
import controllers.modules.robots.insta.InitializeComplexBot1
import javax.inject.Singleton

@Singleton
class RobotsProvider {

  def provide(botSystem: RWSBotController,
              dbConfig: DatabaseConfig[MySQLDriver],
              db: slick.driver.MySQLDriver#Backend#Database,
              configProvider: ConfigProvider) {
    InitializeComplexBot1(botSystem, dbConfig, db, configProvider)
  }

}