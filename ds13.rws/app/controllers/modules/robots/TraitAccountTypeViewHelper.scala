package controllers.modules.robots

import play.api.i18n.Messages
import play.api.mvc.Call
import play.twirl.api.Html

trait TraitAccountTypeViewHelper {

  val id: Long

  def accountsList(
    ownerUser: Option[models.User],
    accounts: Seq[(ds13.bots.storable.Account, models.daos.DBUser)])(
      implicit user: models.User,
      flash: play.api.mvc.Flash,
      messages: Messages): Html

  def profileCall(accountId: Long): Call

  def newAccount(userId: Long)(
    implicit user: models.User,
    flash: play.api.mvc.Flash,
    messages: Messages): Call

}