package controllers.modules.robots

import ds13.bots.BotConfig
import play.api.data.Form
import play.api.i18n.Messages
import play.twirl.api.Html
import ds13.bots.TraitBotLaunchHelper
import ds13.bots.storable.Launch

trait TraitRobotViewHelper {
  
  val id: Long

  def configForm: Form[_]

  def updateConfig(configData: Any, config: BotConfig): Boolean
  
  def configPage(ownerUser: models.User, accountId: Long)(
      implicit user: models.User,
      flash: play.api.mvc.Flash,
      messages: Messages): Html
      
  def statePage(ownerUser: models.User, launch: Launch, blHelper: TraitBotLaunchHelper)(
      implicit user: models.User,
      flash: play.api.mvc.Flash,
      messages: Messages): Html 

}