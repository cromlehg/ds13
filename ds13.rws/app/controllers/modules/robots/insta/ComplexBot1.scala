package controllers.modules.robots.insta

import controllers.modules.robots.LogStorageLogger
import controllers.modules.robots.PlayLoggerWrapper
import controllers.modules.robots.RWSBotController
import controllers.modules.robots.TraitRobotViewHelper
import ds13.bots.BotConfig
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.ConfigProvider
import ds13.bots.storable.insta.complexbot1.ComplexBot1Config
import ds13.bots.storable.insta.complexbot1.ComplexBot1Descriptor
import ds13.bots.storable.insta.storage.slick.mysql.InstaStorableBotMySQLSlickServiceFactory
import ds13.insta.api.intrnl.model.lesfi.LESFICompiler
import play.api.data.Form
import play.api.data.Forms.boolean
import play.api.data.Forms.longNumber
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.number
import play.api.i18n.Messages
import play.twirl.api.Html
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver
import ds13.bots.TraitBotLaunchHelper
import ds13.bots.storable.Launch
import ds13.bots.storable.insta.complexbot1.ComplexBot1LaunchHelper

object InitializeComplexBot1 {

  def apply(
    botSystem: RWSBotController,
    dbConfig: DatabaseConfig[MySQLDriver],
    db: slick.driver.MySQLDriver#Backend#Database,
    configProvider: ConfigProvider): Unit = {

    val instaBotServiceFactory = new RWSInstaServiceFactory(configProvider.path, dbConfig, db)

    val accountTypeId = 1

    val robotId = 1

    val complexBotDescriptor = new ComplexBot1Descriptor(botSystem, robotId, instaBotServiceFactory.copy, accountTypeId, PlayLoggerWrapper())

    botSystem.addDescriptor(complexBotDescriptor)

    botSystem.addRobotViewHelper(new ComplexBot1ViewHelper(robotId))

    botSystem.addAccountTypeViewHelper(new InstaViewHelper(accountTypeId))
  }

}

case class ComplexBot1FormConfig(
  val lesfi: String,
  val isNeedToFillSelfFollowersAsNonToUnfollowAndNonToFollow: Boolean,
  val isNeedToFillSelfFollowingsAsNonToFollow: Boolean,
  val followLimit: Int,
  val isFirstFollow: Boolean,
  val isLoop: Boolean,
  val timeBetweenLoop: Long,
  val isLikeLastPostBeforeFollow: Boolean,
  val isFollowToPrivate: Boolean,
  val isFollowersCountLimit: Boolean,
  val isFollowingsCountLimit: Boolean,
  val followersCountLimit: Int,
  val followingsCountLimit: Int)

class ComplexBot1ViewHelper(override val id: Long) extends TraitRobotViewHelper {

  override def configForm: Form[ComplexBot1FormConfig] = Form(
    mapping(
      ComplexBot1Config.LESFI -> nonEmptyText,
      ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW -> boolean,
      ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW -> boolean,
      ComplexBot1Config.FOLLOW_LIMIT -> number(0, 7000),
      ComplexBot1Config.IS_FIRST_FOLLOW -> boolean,
      ComplexBot1Config.IS_LOOP -> boolean,
      ComplexBot1Config.TIME_BETWEEN_LOOP -> longNumber(0, 2419200000L),
      ComplexBot1Config.IS_LIKE_LASTPOST_BEFORE_FOLLOW -> boolean,
      ComplexBot1Config.IS_FOLLOW_TO_PRIVATE -> boolean,
      ComplexBot1Config.IS_FOLLOWERS_COUNT_LIMIT -> boolean,
      ComplexBot1Config.IS_FOLLOWING_COUNT_LIMIT -> boolean,
      ComplexBot1Config.FOLLOWERS_COUNT_LIMIT -> number(0, 1000000000),
      ComplexBot1Config.FOLLOWING_COUNT_LIMIT -> number(0, 1000000000))(ComplexBot1FormConfig.apply)(ComplexBot1FormConfig.unapply)
      .verifying("Failed form constraints!", fields =>
        fields match {
          case data => new LESFICompiler().checks(data.lesfi)
        }))

  override def updateConfig(configData: Any, config: BotConfig) =
    configData match {
      case c: ComplexBot1FormConfig =>
        config + (ComplexBot1Config.LESFI, c.lesfi)
        config + (ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWINGS_AS_NON_TO_FOLLOW_AND_NON_UNFOLLOW, c.isNeedToFillSelfFollowersAsNonToUnfollowAndNonToFollow)
        config + (ComplexBot1Config.IS_NEED_TO_FILL_SELF_FOLLOWERS_AS_NON_TO_FOLLOW, c.isNeedToFillSelfFollowingsAsNonToFollow)
        config + (ComplexBot1Config.FOLLOW_LIMIT, c.followLimit)
        config + (ComplexBot1Config.IS_FIRST_FOLLOW, c.isFirstFollow)
        config + (ComplexBot1Config.IS_LOOP, c.isLoop)
        config + (ComplexBot1Config.TIME_BETWEEN_LOOP, c.timeBetweenLoop)
        config + (ComplexBot1Config.IS_LIKE_LASTPOST_BEFORE_FOLLOW, c.isLikeLastPostBeforeFollow)
        config + (ComplexBot1Config.IS_FOLLOW_TO_PRIVATE, c.isFollowToPrivate)
        config + (ComplexBot1Config.IS_FOLLOWERS_COUNT_LIMIT, c.isFollowersCountLimit)
        config + (ComplexBot1Config.IS_FOLLOWING_COUNT_LIMIT, c.isFollowingsCountLimit)
        config + (ComplexBot1Config.FOLLOWERS_COUNT_LIMIT, c.followersCountLimit)
        config + (ComplexBot1Config.FOLLOWING_COUNT_LIMIT, c.followingsCountLimit)
        true
      case _ => false
    }

  override def configPage(ownerUser: models.User, accountId: Long)(
    implicit user: models.User,
    flash: play.api.mvc.Flash,
    messages: Messages): Html =
    views.html.admin.robots.insta.complexBot1ConfigPage(ownerUser, accountId, id, configForm)

  override def statePage(ownerUser: models.User, launch: Launch, blHelper: TraitBotLaunchHelper)(
    implicit user: models.User,
    flash: play.api.mvc.Flash,
    messages: Messages): Html =
    views.html.admin.robots.insta.complexBot1StatePage(ownerUser, launch, blHelper.asInstanceOf[ComplexBot1LaunchHelper])

}
