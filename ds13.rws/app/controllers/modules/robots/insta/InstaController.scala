package controllers.modules.robots.insta

import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import play.api.data.Form
import play.api.data.Forms.boolean
import play.api.data.Forms.longNumber
import play.api.data.Forms.mapping
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.number
import javax.inject.Inject
import javax.inject.Singleton
import models.daos.DAO
import ds13.bots.BotSystem
import controllers.modules.robots.RWSBotController
import models.Roles
import controllers.TraitCheckOwnerResourceForUser
import play.api.mvc.Flash
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi
import java.util.UUID
import ds13.common.TraitJSON
import org.json.JSONObject
import ds13.bots.storable.StorableBotLaunchHelper

object InstaAccountConsts {
  val FIELD_USERNAME = "username"
  val FIELD_PASSWORD = "password"
  val FIELD_DEVICE_ID = "deviceId"
  val FIELD_GUID = "GUID"
  val FIELD_UID = "uid"
}

case class InstaAccountConf(
    val username: String,
    val password: String,
    val deviceId: String,
    val GUID: String) extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate(InstaAccountConsts.FIELD_USERNAME, username)
      accumulate(InstaAccountConsts.FIELD_PASSWORD, password)
      accumulate(InstaAccountConsts.FIELD_DEVICE_ID, deviceId)
      accumulate(InstaAccountConsts.FIELD_GUID, GUID)
      accumulate(InstaAccountConsts.FIELD_UID, username)
    })

}

@Singleton
class InstaController @Inject() (override val dao: DAO, val bc: RWSBotController, val messagesApi: MessagesApi) extends TraitCheckOwnerResourceForUser with I18nSupport {

  def configForm: Form[InstaAccountConf] = Form(
    mapping(
      InstaAccountConsts.FIELD_USERNAME -> nonEmptyText,
      InstaAccountConsts.FIELD_PASSWORD -> nonEmptyText,
      InstaAccountConsts.FIELD_DEVICE_ID -> nonEmptyText,
      InstaAccountConsts.FIELD_GUID -> nonEmptyText)(InstaAccountConf.apply)(InstaAccountConf.unapply))

  def configureNewAccount(userId: Long) =
    ActionCheckAdminOrOwnerByUser(userId) { ownerUser =>
      implicit user => implicit request =>
        dao.isExistsFreeActualProxies map { isExists =>
          if (isExists) {
            val form = configForm.fill(InstaAccountConf(
              "",
              "",
              UUID.randomUUID.toString.replaceAll("-", "").substring(0, 16),
              UUID.randomUUID.toString))
            Ok(views.html.admin.subviews.insta.addAccount(ownerUser, form))
          } else {
            Redirect(request headers "referer")
              .flashing("error" -> ("Proxy pool is empty!"))
          }
        }
    }

  def createAccount(userId: Long) =
    ActionCheckAdminOrOwnerByUser(userId) { ownerUser =>
      implicit user => implicit request =>
        configForm.bindFromRequest.fold(
          hasErrors = { form =>
            Future.successful(Ok(views.html.admin.subviews.insta.addAccount(ownerUser, form)).
              flashing(Flash(form.data) + ("error" -> "Проблемы валидации формы")))
          },
          success = { data =>
            dao.assignFreeActualProxyToUser(userId) flatMap { isAssigned =>
              if (isAssigned) {
                dao.createAccount(userId, 1, data.toJSONString).flatMap {
                  case Some(account) =>
                    dao.assignUserProxyToAccount(userId, account.id) map { isAssignedProxy =>
                      if (isAssignedProxy) {
                        Redirect(controllers.routes.AccountsController.profile(account.id))
                          .flashing("success" -> ("Аккаунт был успешно создан!"))
                      } else {
                        Redirect(request headers "referer")
                          .flashing("error" -> ("User proxy pool is empty for " + userId))
                      }
                    }
                  case _ => Future.successful(BadRequest("Some problems during account creation"))
                }
              } else {
                Future.successful(Redirect(request headers "referer")
                  .flashing("error" -> ("Proxy pool is empty!")))
              }
            }
          })
    }

  def getLaunchHelpers(launches: Seq[ds13.bots.storable.Launch]): Seq[(ds13.bots.storable.Launch, Option[StorableBotLaunchHelper])] =
    launches.map(launch => (launch, {
      if (bc.idToLaunchHelpers.containsKey(launch.id)) {
        Some(bc.idToLaunchHelpers.get(launch.id).asInstanceOf[StorableBotLaunchHelper])
      } else {
        None
      }
    }))

  def profile(accountId: Long) =
    ActionCheckAdminOrOwnerByAccount(accountId) { (account, ownerUser) =>
      implicit user => implicit request =>
        dao.getLaunchesPageByAccountId(1, accountId) flatMap { launches =>
          dao.getRobotsPageByAccountTypeId(1, account.accountTypeId) map { robots =>
            Ok(views.html.admin.subviews.insta.profile(ownerUser, account, getLaunchHelpers(launches), robots))
          }
        }
    }

}
