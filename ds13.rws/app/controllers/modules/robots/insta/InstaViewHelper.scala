package controllers.modules.robots.insta

import controllers.modules.robots.TraitAccountTypeViewHelper
import play.api.i18n.Messages
import play.api.mvc.Call
import play.twirl.api.Html

class InstaViewHelper(override val id: Long) extends TraitAccountTypeViewHelper {

  override def accountsList(
    ownerUser: Option[models.User],
    accountsWithOwners: Seq[(ds13.bots.storable.Account, models.daos.DBUser)])(
      implicit user: models.User,
      flash: play.api.mvc.Flash,
      messages: Messages): Html =
    views.html.admin.subviews.insta.accountsList(ownerUser, accountsWithOwners)

  override def profileCall(accountId: Long): Call =
    controllers.modules.robots.insta.routes.InstaController.profile(accountId)
    
  override def newAccount(userId: Long)(
    implicit user: models.User,
    flash: play.api.mvc.Flash,
    messages: Messages): Call =
      controllers.modules.robots.insta.routes.InstaController.configureNewAccount(userId)

}
