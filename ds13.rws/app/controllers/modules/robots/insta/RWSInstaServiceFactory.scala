package controllers.modules.robots.insta

import controllers.modules.robots.LogStorageLogger
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessLogger
import ds13.bots.storable.insta.storage.slick.mysql.InstaStorableBotMySQLSlickServiceFactory
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver

class RWSInstaServiceFactory(
  path: String,
  dbConfig: DatabaseConfig[MySQLDriver],
  db: slick.driver.MySQLDriver#Backend#Database)
    extends InstaStorableBotMySQLSlickServiceFactory(path, dbConfig, db) {

  override def createLogger(config: TraitBotConfig): TraitBotProcessLogger =
    new LogStorageLogger(path)

  override def copy =
    new RWSInstaServiceFactory(path, dbConfig, db)

}