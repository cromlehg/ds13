package models

class AccountStatus(val id: Long, val status: String) {

  override def hashCode = 17 * status.hashCode

  override def equals(obj: Any) = obj match {
    case s: AccountStatus => s.status == status
    case _                => false
  }

  override def toString = status

}

object AccountStatus {

  val ACTIVE = "active"

  val LOCKED = "locked"

  val WAIT_APPROVE = "wait for approve"

  val ID_ACTIVE = 1

  val ID_LOCKED = 2

  val ID_WAIT_APPROVE = 3

  val accountStatuses = Map(
    ID_ACTIVE -> models.AccountStatus.ACTIVE,
    ID_LOCKED -> models.AccountStatus.LOCKED,
    ID_WAIT_APPROVE -> models.AccountStatus.WAIT_APPROVE)

  def apply(id: Long, status: String): AccountStatus =
    new AccountStatus(id, status)

}
