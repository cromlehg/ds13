package models

import play.api.libs.json._
import models.daos.DBTableDefinitions
import ds13.http.TraitProxyProperties
import ds13.common.DummyLogger

class Account(
  val id: Long,
  val ownerId: Long,
  val login: String,
  val email: String,
  val name: Option[String],
  val surname: Option[String],
  val instalogin: String,
  val pass: String,
  val accountId: Option[Long])

class AccountAPI(
  val id: Long,
  val userName: String,
  val accountId: Long,
  val name: String,
  val version: String,
  val deviceId: Option[String],
  val guid: Option[String],
  val proxy: Option[TraitProxyProperties],
  val instaAccountId: Option[Long],
  val instaLogin: String,
  val instaPass: String)

class SimpleProxyProperties(
    val id: Long,
    val ip: String,
    val port: Option[Int],
    val login: Option[String],
    val pass: Option[String]) extends TraitProxyProperties {

  val logger = DummyLogger()

  def getIP: String = ip

  def getPort: Option[Int] = port

  def getLogin: Option[String] = login

  def getPass: Option[String] = pass

}
  
  /*

class SlickMySQLAPIStorage(
    override val accountStorage: SlickMySQLAccountStorage,
    id: Long,
    accountId: Long,
    name: String,
    version: String,
    deviceId: Option[String],
    guid: Option[String],
    proxyId: Option[Long]) extends TraitAPIStorage with ResultSolver {

  override val logger = accountStorage.logger

  val dao = accountStorage.dao

  override def getProxyOpts: Option[TraitProxyProperties] =
    proxyId flatMap { prxId =>
      solved(dao.getProxyDataById(prxId).map(_.map {
        case (id, ip, port, login, pass) =>
          new SlickMySQLProxyProperties(logger, this, id, ip, port, login, pass)
      }))
    }

  override def getAccountId: Long = accountId

  override def getDeviceId: Option[String] = deviceId

  override def getGUID: Option[String] = guid

}

*/