package models

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.tz.DateTimeZoneBuilder
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import java.util.Locale

class User(id: Long,
           val login: String,
           val email: String,
           val hash: Option[String],
           val roles: List[String],
           val avatar: Option[models.Media],
           val userStatus: UserStatus,
           val accountStatus: AccountStatus,
           val name: Option[String],
           val surname: Option[String],
           val timezone: models.Timezone,
           val registered: Long,
           val balance: Long,
           val plan: models.daos.DBPlan,
           val planEnd: Option[Long],
           val registerToken: Option[String],
           val registerTokenDate: Option[Long],
           val recoverToken: Option[String],
           val recoverTokenDate: Option[Long]) extends ds13.bots.storable.User(id) {

  val ldt = new LocalDateTime(registered * 1000L, DateTimeZone.UTC)

  val ldtPlanEnd = planEnd.map(t => new LocalDateTime(t * 1000L, DateTimeZone.UTC))

  override def equals(obj: Any) = obj match {
    case user: User => user.email == email
    case _          => false
  }

  override def toString = email

  def getRegistered(zone: String): DateTime = getRegistered.toDateTime(DateTimeZone forID zone)

  def getRegistered: LocalDateTime = ldt

  def getPlanEnd(zone: String): Option[DateTime] = getPlanEnd.map(t => t.toDateTime(DateTimeZone forID zone))

  def getPlanEnd: Option[LocalDateTime] = ldtPlanEnd

  def isAdmin = roles.contains(Roles.ADMIN)

  def toDBUser =
    models.daos.DBUser(
      id,
      login,
      email,
      hash,
      avatar.map(_.id),
      userStatus.id.toInt,
      accountStatus.id.toInt,
      name,
      surname,
      timezone.id,
      registered,
      balance,
      registerToken,
      registerTokenDate,
      recoverToken,
      recoverTokenDate,
      plan.id,
      planEnd)

}

object DummyAdminUser {

  def apply(): User =
    new User(
      1,
      "",
      "",
      None,
      List(Roles.ADMIN),
      None,
      UserStatus(2, UserStatus.ONLINE),
      AccountStatus(2, AccountStatus.ACTIVE),
      None,
      None,
      new models.Timezone(1, "Europe/Moscow"),
      0,
      0,
      new models.daos.DBPlan(0, "", None, None),
      None,
      None,
      None,
      None,
      None)

}
