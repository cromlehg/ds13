package models.daos

import scala.concurrent.Future

import javax.inject.Inject
import models.AccountStatus
import models.Param
import models.Timezone
import models.User
import models.UserStatus
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import controllers.MediaUtils
import models.UserStatus
import slick.driver.MySQLDriver
import ds13.common.DummyLogger
import slick.driver.JdbcProfile
import models.AccountAPI
import models.SimpleProxyProperties
import ds13.http.TraitProxyProperties
import java.sql.Blob
import ds13.common.TraitId
import org.mindrot.jbcrypt.BCrypt
import models.Roles
import slick.dbio.DBIOAction
import ds13.bots.storable.ProxyAssign

/**
 *
 * Queries with SlickBUG should be replace leftJoin with for comprehesive. Bug:
 * "Unreachable reference to after resolving monadic joins"
 *
 */

// inject this 
// conf: play.api.Configuration, 
// and then get conf value
// conf.underlying.getString(Utils.meidaPath)
class DAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider, mediaUtils: MediaUtils) extends DAOSlick with TraitSolved {

  import driver.api._

  val pageSize = 10

  val timezones = Map(1 -> "Europe/Moscow")

  val userStatuses = Map(1 -> models.UserStatus.ONLINE, 2 -> models.UserStatus.OFFLINE)

  def param(name: String) =
    db.run(params.filter(_.name === name).result.headOption).map(_.map(p => new Param(p.id, p.name, p.value)))

  def getAllPlans(): Future[Seq[DBPlan]] =
    db.run(plans.result)

  def isExistsPlan(id: Long): Future[Boolean] =
    db.run(plans.filter(_.id === id).exists.result)

  def getFirstNotAssignedProxy: Future[Option[ds13.bots.storable.Proxy]] = {
    val q = sql"SELECT * FROM proxies AS a WHERE NOT EXISTS(SELECT * FROM proxy_assigns AS b WHERE a.id = b.proxy_id) LIMIT 1"
      .as[(Long, String, Int, Option[Int], Option[String], Option[String])].headOption
    db.run(q) map (_ map (t => ds13.bots.storable.Proxy(t._1, t._2, t._3, t._4, t._5, t._6)))
  }

  def isExistsFreeActualProxies: Future[Boolean] =
    getFirstNotAssignedProxy.map(_.isDefined)

  def getPlanById(id: Long): Future[Option[DBPlan]] =
    db.run(plans.filter(_.id === id).result.headOption)

  def isExistsProxyByIP(ip: String): Future[Boolean] =
    db.run(proxies.filter(_.ip === ip).exists.result)

  def getLaunchesAssignedWithProxy(id: Long): Future[Seq[ds13.bots.storable.ProxyAssign]] =
    db.run(proxyAssigns.filter(t => t.proxyId === id && t.launchId.nonEmpty).result)

  def assignFreeActualProxyToUser(userId: Long): Future[Boolean] = {
    getFirstNotAssignedProxy flatMap (_ match {
      case Some(proxy) =>
        val query = for {
          dbProxyAssign <- (proxyAssigns returning proxyAssigns.map(_.id) into ((v, id) => v.copy(id = id))) +=
            ds13.bots.storable.ProxyAssign(0, proxy.id, Some(userId), None, None)
        } yield dbProxyAssign
        db.run(query.transactionally) map (_ => true)
      case _ => Future.successful(false)
    })
  }

  def removeProxyAssigns(proxyId: Long): Future[Int] =
    db.run(proxyAssigns.filter(_.proxyId === proxyId).delete.transactionally)

  def removeProxy(proxyId: Long): Future[Boolean] =
    db.run(proxies.filter(_.id === proxyId).delete.transactionally.map(_ > 0))

  def removeProxyWithAssigns(proxyId: Long): Future[Boolean] =
    removeProxyAssigns(proxyId) flatMap { t => removeProxy(proxyId) }

  def removeProxyAssignFromAccount(accountId: Long) =
    db.run(proxyAssigns.filter(_.accountId === accountId).delete.transactionally).map(_ > 0)

  def assignUserProxyToAccount(userId: Long, accountId: Long): Future[Boolean] =
    db.run(proxyAssigns.filter(t => t.userId === userId && t.accountId.isEmpty).map(t => (t.accountId)).update(Some(accountId)).transactionally) map (r => if (r == 1) true else false)

  def getLaunchesPageByUserIdAndAccountTypeId(pageId: Long, userId: Long, accountTypeId: Long): Future[Seq[ds13.bots.storable.Launch]] = {
    val query = for {
      dbAccount <- accounts.filter(t => t.ownerId === userId && t.accountTypeId === accountTypeId)
      dbLaunch <- launches.filter(_.accountId === dbAccount.accountTypeId)
    } yield dbLaunch
    db.run(query.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)
  }

  def getLaunchesGroupedPage(pageId: Long): Future[Seq[(ds13.bots.storable.Launch, DBUser, ds13.bots.storable.AccountType, ds13.bots.storable.Account)]] = {
    val query = for {
      dbLaunch <- launches.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbAccount <- accounts.filter(_.id === dbLaunch.accountId)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbLaunch, dbUser, dbAccountType, dbAccount)
    db.run(query.result)
  }

  def getLaunchesGroupedPageByUserId(pageId: Long, userId: Long): Future[Seq[(ds13.bots.storable.Launch, DBUser, ds13.bots.storable.AccountType, ds13.bots.storable.Account)]] = {
    val query = for {
      dbAccount <- accounts.filter(_.ownerId === userId)
      dbLaunch <- launches.filter(_.accountId === dbAccount.accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbLaunch, dbUser, dbAccountType, dbAccount)
    db.run(query.result)
  }

  def getLaunchesGroupPageByUserIdAndAccountTypeId(pageId: Long, userId: Long, accountTypeId: Long): Future[Seq[(ds13.bots.storable.Launch, DBUser, ds13.bots.storable.AccountType, ds13.bots.storable.Account)]] = {
    val query = for {
      dbAccount <- accounts.filter(t => t.ownerId === userId && t.accountTypeId === accountTypeId)
      dbLaunch <- launches.filter(_.accountId === dbAccount.accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbLaunch, dbUser, dbAccountType, dbAccount)
    db.run(query.result)
  }

  def getUsersPageByRobotId(pageId: Long, robotId: Long): Future[Seq[models.User]] = {
    val query = for {
      dbRobot <- robots.filter(_.id === robotId)
      dbAccount <- accounts.filter(_.accountTypeId === dbRobot.accountTypeId)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbPlan <- plans.filter(_.id === dbUser.planId)
    } yield (dbUser, dbPlan)
    db.run(query.sortBy(_._1.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result) map (_ map { case (a, b) => userFrom(a, b) })
  }

  def getAccountsWithOwnersPageByRobotId(pageId: Long, robotId: Long): Future[Seq[(ds13.bots.storable.Account, DBUser)]] = {
    val query = for {
      dbRobot <- robots.filter(_.id === robotId)
      dbAccount <- accounts.filter(_.accountTypeId === dbRobot.accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
    } yield (dbAccount, dbUser)
    db.run(query.result)
  }

  def getATUPageByRobotId(pageId: Long, robotId: Long): Future[Seq[(ds13.bots.storable.Account, ds13.bots.storable.AccountType, DBUser)]] = {
    val query = for {
      dbRobot <- robots.filter(_.id === robotId)
      dbAccount <- accounts.filter(_.accountTypeId === dbRobot.accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbAccount, dbAccountType, dbUser)
    db.run(query.result)
  }

  def getATUPageByUserId(pageId: Long, userId: Long): Future[Seq[(ds13.bots.storable.Account, ds13.bots.storable.AccountType, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.filter(_.ownerId === userId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbAccount, dbAccountType, dbUser)
    db.run(query.result)
  }

  def getAccountsPageByRobotId(pageId: Long, robotId: Long): Future[Seq[ds13.bots.storable.Account]] = {
    val query = for {
      dbRobot <- robots.filter(_.id === robotId)
      dbAccount <- accounts.filter(_.accountTypeId === dbRobot.accountTypeId)
    } yield dbAccount
    db.run(query.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)
  }

  def getRobotsPageByAccountTypeId(pageId: Long, accountTypeId: Long): Future[Seq[ds13.bots.storable.Robot]] = {
    val query = for {
      dbRobot <- robots.filter(_.accountTypeId === accountTypeId)
    } yield dbRobot
    db.run(query.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)
  }

  def getLaunchesPageByUserId(pageId: Long, userId: Long): Future[Seq[ds13.bots.storable.Launch]] = {
    val query = for {
      dbAccount <- accounts.filter(_.ownerId === userId)
      dbLaunch <- launches.filter(_.accountId === dbAccount.accountTypeId)
    } yield dbLaunch
    db.run(query.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)
  }

  def getPlansPage(pageId: Long): Future[Seq[DBPlan]] =
    db.run(plans.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getRobotsPage(pageId: Long): Future[Seq[ds13.bots.storable.Robot]] =
    db.run(robots.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  //  def getProxiesPageFullInfo(pageId: Long): Future[Seq[(ds13.bots.storable.Proxy, Option[DBUser], Option[ds13.bots.storable.Account], Option[ds13.bots.storable.Launch])]] = {
  //    val query = for {
  //      dbProxy <- proxies.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
  //      dbProxyAssign <- proxyAssigns.filter(_.proxyId === dbProxy.id)      
  //    } yield (dbProxy, None, None, None)
  //    db.run(query.result)
  //  }

  //  def getProxiesPageFullInfo(pageId: Long): Future[Seq[(ds13.bots.storable.Proxy, DBUser, ds13.bots.storable.Account, ds13.bots.storable.Launch)]] = {
  //    val query = for {
  //      dbProxy <- proxies.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
  //      //(c, s) <- coffees join suppliers on (_.supID === _.id)
  //      dbProxyAssign <- proxyAssigns.filter(_.proxyId === dbProxy.id)
  //      dbUser <- users.filter(_.id === dbProxyAssign.userId)
  //      dbAccount <- accounts.filter(_.id === dbProxyAssign.accountId)
  //      dbLaunch <- launches.filter(_.id === dbProxyAssign.launchId)
  //    } yield (dbProxy, dbUser, dbAccount, dbLaunch)
  //    db.run(query.result)
  //  }

  def getProxiesPageFullInfo(pageId: Long): Future[Seq[(ds13.bots.storable.Proxy, Option[DBUser], Option[ds13.bots.storable.Account], Option[ds13.bots.storable.Launch])]] = {
    val query = for {
      ((((dbProxy, dbProxyAssignOpt), dbUserOpt), dbAccountOpt), dbLaunchOpt) <- proxies.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
        .joinLeft(proxyAssigns).on(_.id === _.proxyId)
        .joinLeft(users).on(_._2.flatMap(_.userId) === _.id)
        .joinLeft(accounts).on(_._1._2.flatMap(_.accountId) === _.id)
        .joinLeft(launches).on(_._1._1._2.flatMap(_.launchId) === _.id)
    } yield (dbProxy, dbUserOpt, dbAccountOpt, dbLaunchOpt)
    db.run(query.result)
  }

  def getProxiesPage(pageId: Long): Future[Seq[ds13.bots.storable.Proxy]] =
    db.run(proxies.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getLaunchById(launchId: Long): Future[Option[ds13.bots.storable.Launch]] =
    db.run(launches.filter(_.id === launchId).result.headOption)

  def getLaunchesPage(pageId: Long): Future[Seq[ds13.bots.storable.Launch]] =
    db.run(launches.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getLaunchesPageByAccountId(pageId: Long, accountId: Long): Future[Seq[ds13.bots.storable.Launch]] =
    db.run(launches.filter(_.accountId === accountId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getLaunchesGroupedPageByAccountId(pageId: Long, accountId: Long): Future[Seq[(ds13.bots.storable.Launch, DBUser, ds13.bots.storable.AccountType, ds13.bots.storable.Account)]] = {
    val query = for {
      dbLaunch <- launches.filter(_.accountId === accountId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbAccount <- accounts.filter(_.id === accountId)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbLaunch, dbUser, dbAccountType, dbAccount)
    db.run(query.result)
  }

  def deleteAccount(id: Long): Future[Boolean] =
    db.run(accounts.filter(_.id === id).delete.transactionally) map { _ == 1 }

  def getAccountById(acccountId: Long): Future[Option[ds13.bots.storable.Account]] =
    db.run(accounts.filter(_.id === acccountId).result.headOption)

  def getRobotById(robotId: Long): Future[Option[ds13.bots.storable.Robot]] =
    db.run(robots.filter(_.id === robotId).result.headOption)

  def getAccountTypeById(acccountTypeId: Long): Future[Option[ds13.bots.storable.AccountType]] =
    db.run(accountTypes.filter(_.id === acccountTypeId).result.headOption)

  def getAccountsPage(pageId: Long): Future[Seq[ds13.bots.storable.Account]] =
    db.run(accounts.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getAccountsWithOwnersPage(pageId: Long): Future[Seq[(ds13.bots.storable.Account, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.id)
    } yield (dbAccount, dbUser)
    db.run(query.result)
  }

  def getATUPage(pageId: Long): Future[Seq[(ds13.bots.storable.Account, ds13.bots.storable.AccountType, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbAccount, dbAccountType, dbUser)
    db.run(query.result)
  }

  def getAccountsPageByUserId(pageId: Long, userId: Long): Future[Seq[ds13.bots.storable.Account]] =
    db.run(accounts.filter(_.ownerId === userId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getAccountsWithOwnersPageByUserId(pageId: Long, userId: Long): Future[Seq[(ds13.bots.storable.Account, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.filter(_.ownerId === userId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
    } yield (dbAccount, dbUser)
    db.run(query.result)
  }

  def getAccountsPageByUserIdAndAccountTypeId(pageId: Long, userId: Long, accountTypeId: Long): Future[Seq[ds13.bots.storable.Account]] =
    db.run(accounts.filter(t => t.ownerId === userId && t.accountTypeId === accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getAccountsWithOwnersPageByUserIdAndAccountTypeId(pageId: Long, userId: Long, accountTypeId: Long): Future[Seq[(ds13.bots.storable.Account, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.filter(t => t.ownerId === userId && t.accountTypeId === accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
    } yield (dbAccount, dbUser)
    db.run(query.result)
  }

  def getAccountTypesPage(pageId: Long): Future[Seq[ds13.bots.storable.AccountType]] =
    db.run(accountTypes.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getAccountsPageByAccountTypeId(pageId: Long, accountTypeId: Long): Future[Seq[ds13.bots.storable.Account]] =
    db.run(accounts.filter(_.accountTypeId === accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getAccountsWithOwnersPageByAccountTypeId(pageId: Long, accountTypeId: Long): Future[Seq[(ds13.bots.storable.Account, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.filter(_.accountTypeId === accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
    } yield (dbAccount, dbUser)
    db.run(query.result)
  }

  def getATUPageByAccountTypeId(pageId: Long, accountTypeId: Long): Future[Seq[(ds13.bots.storable.Account, ds13.bots.storable.AccountType, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.filter(_.accountTypeId === accountTypeId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield (dbAccount, dbAccountType, dbUser)
    db.run(query.result)
  }

  def getAccountsPageByAccountTypeIdAndUserId(pageId: Long, accountTypeId: Long, userId: Long): Future[Seq[ds13.bots.storable.Account]] =
    db.run(accounts.filter(t => t.accountTypeId === accountTypeId && t.ownerId === userId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)

  def getAccountsWithOwnersPageByAccountTypeIdAndUserId(pageId: Long, accountTypeId: Long, userId: Long): Future[Seq[(ds13.bots.storable.Account, DBUser)]] = {
    val query = for {
      dbAccount <- accounts.filter(t => t.accountTypeId === accountTypeId && t.ownerId === userId).sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
    } yield (dbAccount, dbUser)
    db.run(query.result)
  }

  def getAccountTypesPageByUserId(pageId: Long, userId: Long): Future[Seq[ds13.bots.storable.AccountType]] = {
    val query = for {
      dbAccount <- accounts.filter(_.ownerId === userId)
      dbAccountType <- accountTypes.filter(_.id === dbAccount.accountTypeId)
    } yield dbAccountType
    db.run(query.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize).result)
  }

  def getUserByLaunchId(launchId: Long): Future[Option[models.User]] = {
    val query = for {
      dbLaunch <- launches.filter(_.id === launchId)
      dbAccount <- accounts.filter(_.id === dbLaunch.accountId)
      dbUser <- users.filter(_.id === dbAccount.ownerId)
      dbPlan <- plans.filter(_.id === dbUser.planId)
    } yield (dbUser, dbPlan)
    getUserFromQuery(query)
  }

  def getUsersPage(pageId: Long): Future[Seq[models.User]] = {
    val query = for {
      dbUser <- users.sortBy(_.id.desc).drop(if (pageId > 0) pageSize * (pageId - 1) else 0).take(pageSize)
      dbPlan <- plans.filter(_.id === dbUser.planId)
    } yield (dbUser, dbPlan)
    db.run(query.result) map (_ map { case (a, b) => userFrom(a, b) })
  }

  def findMediaByIdWthoutOwner(id: Long) =
    db.run(media.filter(_.id === id).result.headOption).map(_.map { m =>
      new models.Media(mediaUtils.getFilesFolderAbs, m.id, None, m.path, m.mimeType, m.created)
    })

  def findUserById(id: Long) = {
    val query = for {
      dbUser <- users.filter(_.id === id)
      dbPlan <- plans.filter(_.id === dbUser.planId)
    } yield (dbUser, dbPlan)
    getUserFromQuery(query)
  }

  def findActiveUserById(id: Long) = {
    val query = for {
      dbUser <- users.filter(t => t.id === id && t.accountStatusId === AccountStatus.ID_ACTIVE)
      dbPlan <- plans.filter(_.id === dbUser.planId)
    } yield (dbUser, dbPlan)
    getUserFromQuery(query)
  }

  def findUserByEmail(email: String): Future[Option[User]] = {
    val query = for {
      dbUser <- users.filter(_.email === email)
      dbPlan <- plans.filter(_.id === dbUser.planId)
    } yield (dbUser, dbPlan)
    getUserFromQuery(query)
  }

  def getUserFromQuery(query: Query[(Users, Plans), (DBUser, DBPlan), Seq]) =
    db.run(query.result.headOption).map(_.map { case (a, b) => userFrom(a, b) })

  def userFrom(dbUser: DBUser, dbPlan: DBPlan) =
    new User(
      dbUser.id,
      dbUser.login,
      dbUser.email,
      dbUser.hash,
      List(),
      None,
      new UserStatus(dbUser.userStatusId, userStatuses(dbUser.userStatusId)),
      new AccountStatus(dbUser.accountStatusId, AccountStatus.accountStatuses(dbUser.accountStatusId)),
      dbUser.name,
      dbUser.surname,
      new Timezone(dbUser.timezoeId, timezones(dbUser.timezoeId)),
      dbUser.registered,
      dbUser.balance,
      dbPlan,
      dbUser.planEnd,
      dbUser.reigsterToken,
      dbUser.reigsterTokenDate,
      dbUser.recoverToken,
      dbUser.recoverTokenDate)

  def userFrom(dbUser: DBUser, dbPlan: DBPlan, roles: String*) =
    new User(
      dbUser.id,
      dbUser.login,
      dbUser.email,
      dbUser.hash,
      roles.toList,
      None,
      new UserStatus(dbUser.userStatusId, userStatuses(dbUser.userStatusId)),
      new AccountStatus(dbUser.accountStatusId, AccountStatus.accountStatuses(dbUser.accountStatusId)),
      dbUser.name,
      dbUser.surname,
      new Timezone(dbUser.timezoeId, timezones(dbUser.timezoeId)),
      dbUser.registered,
      dbUser.balance,
      dbPlan,
      dbUser.planEnd,
      dbUser.reigsterToken,
      dbUser.reigsterTokenDate,
      dbUser.recoverToken,
      dbUser.recoverTokenDate)

  def findActiveUserWithRolesById(userId: Long): Future[Option[User]] =
    updateUserWithRoles(findActiveUserById(userId))

  def findUserWithRolesById(userId: Long): Future[Option[User]] =
    updateUserWithRoles(findUserById(userId))

  def createProxy(ip: String, port: Option[Int], authType: Int, login: Option[String], pass: Option[String]): Future[Option[ds13.bots.storable.Proxy]] = {
    val query = for {
      dbProxy <- (proxies returning proxies.map(_.id) into ((v, id) => v.copy(id = id))) += ds13.bots.storable.Proxy(0, ip, authType, port, login, pass)
    } yield dbProxy
    db.run(query.transactionally) map (s => Some(s))
  }

  def createAccount(userId: Long, accountTypeId: Long, properties: Option[String]): Future[Option[ds13.bots.storable.Account]] = {
    val query = for {
      dbAccount <- (accounts returning accounts.map(_.id) into ((v, id) => v.copy(id = id))) += ds13.bots.storable.Account(0, userId, accountTypeId, properties)
    } yield dbAccount
    db.run(query.transactionally) map (s => Some(s))
  }

  def isLoginExists(login: String): Future[Boolean] =
    db.run(users.filter(_.login === login.trim.toLowerCase).result.headOption) map (_.isDefined)

  def isEmailExists(email: String): Future[Boolean] =
    db.run(users.filter(_.email === email.trim.toLowerCase).result.headOption) map (_.isDefined)

  def removeUserWithRoles(userId: Long) =
    db.run(userRoles.filter(_.userId === userId).delete.andThen(users.filter(_.id === userId).delete).transactionally).map(_ > 0)

  def createUser(
    login: String,
    email: String,
    password: String,
    name: Option[String],
    surname: Option[String],
    isAdmin: Boolean,
    balance: Long,
    planId: Long,
    planEnd: Option[Long],
    reigsterToken: Option[String],
    reigsterTokenDate: Option[Long],
    recoverToken: Option[String],
    recoverTokenDate: Option[Long]): Future[Option[models.User]] = {
    val query = for {
      dbUser <- (users returning users.map(_.id) into ((v, id) => v.copy(id = id))) += new models.daos.DBUser(
        0,
        login,
        email,
        Some(BCrypt.hashpw(password, BCrypt.gensalt())),
        None,
        1,
        1,
        name,
        surname,
        1,
        System.currentTimeMillis(),
        balance,
        reigsterToken,
        reigsterTokenDate,
        recoverToken,
        recoverTokenDate,
        planId,
        planEnd)
    } yield dbUser
    println(query)
    db.run(query.transactionally) flatMap { dbUser =>
      if (isAdmin)
        addRolesToUser(dbUser.id, Roles.ADMIN, Roles.CLIENT) flatMap { t =>
          getPlanById(dbUser.planId) map (_ map { dbPlan =>
            userFrom(dbUser, dbPlan, Roles.ADMIN, Roles.CLIENT)
          })
        }
      else
        addRolesToUser(dbUser.id, Roles.CLIENT) flatMap { t =>
          getPlanById(dbUser.planId) map (_ map { dbPlan =>
            userFrom(dbUser, dbPlan, Roles.CLIENT)
          })
        }
    }
  }

  def addRolesToUser(userId: Long, roles: String*): Future[Unit] =
    db.run(DBIO.seq {
      userRoles ++= roles.map(r => DBUserRole(userId, r))
    }.transactionally)

  def updateUserWithRoles(futureOptUser: Future[Option[User]]): Future[Option[User]] =
    futureOptUser flatMap {
      case Some(u) =>
        findRolesByUserId(u.id).map { r =>
          Some(
            new User(
              u.id,
              u.login,
              u.email,
              u.hash,
              r.toList,
              None,
              u.userStatus,
              u.accountStatus,
              u.name,
              u.surname,
              u.timezone,
              u.registered,
              u.balance,
              u.plan,
              u.planEnd,
              u.registerToken,
              u.registerTokenDate,
              u.recoverToken,
              u.recoverTokenDate))

        }
      case None => Future(None)
    }

  def findRolesByUserId(userId: Long) =
    db.run(userRoles.filter(_.userId === userId).result).map(_.map(_.role))

  def insertMedia(m: models.Media): Future[Option[models.Media]] = {
    val query = for {
      dbMedia <- (media returning media.map(_.id) into ((v, id) => v.copy(id = id))) +=
        new DBMedia(
          m.id,
          m.owner.flatMap { t => Some(t.id) },
          m.path,
          m.mimeType,
          m.created)
    } yield dbMedia
    db.run(query.transactionally) map (s =>
      Some(new models.Media(mediaUtils.getFilesFolderAbs, s.id, None, s.path, s.mimeType, s.created)))
  }

  ////////////// HELPERS ////////////////

  @inline final def someToSomeFlatMap[T1, T2](f1: Future[Option[T1]], f2: T1 => Future[Option[T2]]): Future[Option[T2]] =
    f1 flatMap (_ match {
      case Some(r) => f2(r)
      case None    => Future.successful(None)
    })

  @inline final def someToSomeFlatMapElse[T](f1: Future[Option[_]], f2: Future[Option[T]]): Future[Option[T]] =
    f1 flatMap (_ match {
      case Some(r) => Future.successful(None)
      case None    => f2
    })

  @inline final def someToBooleanFlatMap[T](f1: Future[Option[T]], f2: T => Future[Boolean]): Future[Boolean] =
    f1 flatMap (_ match {
      case Some(r) => f2(r)
      case None    => Future.successful(false)
    })

  @inline final def someToSeqFlatMap[T1, T2](f1: Future[Option[T1]], f2: T1 => Future[Seq[T2]]): Future[Seq[T2]] =
    f1 flatMap (_ match {
      case Some(r) => f2(r)
      case None    => Future.successful(Seq.empty[T2])
    })

  @inline final def seqToSeqFlatMap[T1, T2](f1: Future[Seq[T1]], f2: T1 => Future[T2]): Future[Seq[T2]] =
    f1 flatMap { rs =>
      Future.sequence {
        rs map { r =>
          f2(r)
        }
      }
    }

}
