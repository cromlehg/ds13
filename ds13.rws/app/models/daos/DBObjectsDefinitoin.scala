package models.daos

import ds13.common.TraitId

case class DBMedia(
  override val id: Long,
  ownerId: Option[Long],
  path: String,
  mimeType: Option[String],
  created: Long) extends TraitId

case class DBUser(
  override val id: Long,
  login: String,
  email: String,
  hash: Option[String],
  avatarId: Option[Long],
  userStatusId: Int,
  accountStatusId: Int,
  name: Option[String],
  surname: Option[String],
  timezoeId: Int,
  registered: Long,
  balance: Long,
  reigsterToken: Option[String],
  reigsterTokenDate: Option[Long],
  recoverToken: Option[String],
  recoverTokenDate: Option[Long],
  planId: Long,
  planEnd: Option[Long]) extends TraitId

case class DBSession(
  override val id: Long,
  userId: Long,
  suid: String) extends TraitId

case class DBUserRole(
  userId: Long,
  role: String)

case class DBParam(
  override val id: Long,
  name: String,
  value: String) extends TraitId

case class DBPlan(
  override val id: Long,
  name: String,
  price: Option[Long],
  period: Option[Int]) extends TraitId

case class DBTimezone(
  override val id: Long,
  timezone: String) extends TraitId


