package models.daos

import slick.driver.JdbcProfile
import slick.lifted.ProvenShape.proveShapeOf

trait DBTableDefinitions {

  protected val driver: JdbcProfile
  import driver.api._

  class Media(tag: Tag) extends Table[DBMedia](tag, "media") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def ownerId = column[Option[Long]]("owner_id")
    def path = column[String]("path")
    def mimeType = column[Option[String]]("mime_type")
    def created = column[Long]("created")
    def * = (id,
      ownerId,
      path,
      mimeType,
      created) <> (DBMedia.tupled, DBMedia.unapply)
  }

  class Users(tag: Tag) extends Table[DBUser](tag, "users") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def login = column[String]("login")
    def email = column[String]("email")
    def hash = column[Option[String]]("hash")
    def avatarId = column[Option[Long]]("avatar_id")
    def userStatusId = column[Int]("user_status_id")
    def accountStatusId = column[Int]("account_status_id")
    def name = column[Option[String]]("name")
    def surname = column[Option[String]]("surname")
    def timezoneId = column[Int]("timezone_id")
    def registered = column[Long]("registered")
    def balance = column[Long]("balance")
    def reigsterToken = column[Option[String]]("register_token")
    def reigsterTokenDate = column[Option[Long]]("register_token_date")
    def recoverToken = column[Option[String]]("recover_token")
    def recoverTokenDate = column[Option[Long]]("recover_token_date")
    def planId = column[Long]("plan_id")
    def planEnd = column[Option[Long]]("plan_end")
    def * = (id,
      login,
      email,
      hash,
      avatarId,
      userStatusId,
      accountStatusId,
      name,
      surname,
      timezoneId,
      registered,
      balance,
      reigsterToken,
      reigsterTokenDate,
      recoverToken,
      recoverTokenDate,     
      planId,
      planEnd) <> (DBUser.tupled, DBUser.unapply)
  }

  class Sessions(tag: Tag) extends Table[DBSession](tag, "sessions") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[Long]("user_id")
    def suid = column[String]("suid")
    def * = (id, userId, suid) <> (DBSession.tupled, DBSession.unapply)
  }

  class UserRoles(tag: Tag) extends Table[DBUserRole](tag, "user_roles") {
    def userId = column[Long]("user_id")
    def role = column[String]("role")
    def * = (userId, role) <> (DBUserRole.tupled, DBUserRole.unapply)
  }

  class Params(tag: Tag) extends Table[DBParam](tag, "params") {
    def id = column[Long]("id")
    def name = column[String]("name")
    def value = column[String]("value")
    def * = (id, name, value) <> (DBParam.tupled, DBParam.unapply)
  }
  
  class Plans(tag: Tag) extends Table[DBPlan](tag, "plans") {
    def id = column[Long]("id")
    def name = column[String]("name")
    def price = column[Option[Long]]("price")
    def period = column[Option[Int]]("period")
    def * = (id, name, price, period) <> (DBPlan.tupled, DBPlan.unapply)
  }

  val users = TableQuery[Users]

  val media = TableQuery[Media]

  val sessions = TableQuery[Sessions]

  val userRoles = TableQuery[UserRoles]

  val params = TableQuery[Params]
  
  val plans = TableQuery[Plans]

  val accountTypes = TableQuery[ds13.bots.storable.storage.slick.mysql.AccountTypes]

  val accounts = TableQuery[ds13.bots.storable.storage.slick.mysql.Accounts]

  val robots = TableQuery[ds13.bots.storable.storage.slick.mysql.Robots]

  val launches = TableQuery[ds13.bots.storable.storage.slick.mysql.Launches]

  val proxies = TableQuery[ds13.bots.storable.storage.slick.mysql.Proxies]

  val proxyAssigns = TableQuery[ds13.bots.storable.storage.slick.mysql.ProxyAssigns]

}

