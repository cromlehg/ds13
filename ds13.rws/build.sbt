name := """rws"""
organization := "ds13"

version := "0.2-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

resolvers += "DS13" at "http://maven.siamway.ru/"

libraryDependencies += filters

libraryDependencies ++= Seq(
  "ds13" %% "bots-storable-insta" % "0.2-SNAPSHOT",
  cache,
  ws,

  "org.scalatestplus.play" % "scalatestplus-play_2.11" % "3.0.0-M3" % Test,
  "mysql" % "mysql-connector-java" % "6.0.6",
  "com.typesafe.play" %% "play-slick" % "3.0.0-M4",

  "com.github.nscala-time" %% "nscala-time" % "2.16.0",

  "org.webjars" % "jquery" % "2.2.1",
  "org.webjars" % "bootstrap" % "3.3.6" exclude("org.webjars", "jquery"),

  "org.webjars" % "x-editable-bootstrap3" % "1.5.1-1",

  "com.github.t3hnar" %% "scala-bcrypt" % "2.6",
  "org.webjars" % "respond" % "1.4.2",
  "org.webjars" % "html5shiv" % "3.7.3",
  "org.webjars" % "font-awesome" % "4.6.1",
  "org.webjars" % "ionicons" % "2.0.1",
  "org.webjars" % "dropzone" % "4.2.0",
  "org.webjars.bower" % "iCheck" % "1.0.2",
  
  "jp.t2v" %% "play2-auth"        % "0.14.2"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

includeFilter in (Assets, LessKeys.less) := "*.less"
scalacOptions += "-feature"

