CREATE TABLE media (
  id                        SERIAL PRIMARY KEY,
  owner_id                  BIGINT UNSIGNED,
  path                      VARCHAR(190) NOT NULL,
  mime_type                 VARCHAR(100),
  created                   BIGINT UNSIGNED NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE users (
  id                        SERIAL PRIMARY KEY,
  login                     VARCHAR(100) NOT NULL UNIQUE,
  email                     VARCHAR(100) NOT NULL UNIQUE,
  hash                      VARCHAR(60),
  avatar_id                 BIGINT,
  user_status_id            INT UNSIGNED NOT NULL,
  account_status_id         INT UNSIGNED NOT NULL,
  name                      VARCHAR(100),
  surname                   VARCHAR(100),
  timezone_id               INT UNSIGNED NOT NULL,
  registered                BIGINT UNSIGNED NOT NULL,
  balance                   BIGINT NOT NULL,
  register_token            VARCHAR(100),
  register_token_date       BIGINT UNSIGNED,
  recover_token             VARCHAR(100),
  recover_token_date        BIGINT UNSIGNED,
  plan_id                   BIGINT UNSIGNED NOT NULL,
  plan_end                  BIGINT UNSIGNED
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE user_roles (
  user_id                   BIGINT UNSIGNED NOT NULL,
  role                      VARCHAR(100) NOT NULL,
  PRIMARY KEY (user_id, role)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE params (
  id                        SERIAL PRIMARY KEY,
  name                      VARCHAR(100) NOT NULL UNIQUE,
  `value`                   VARCHAR(100)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE plans (
  id                        SERIAL PRIMARY KEY,
  name                      VARCHAR(100) NOT NULL UNIQUE,
  price                     BIGINT UNSIGNED NOT NULL,
  period                    INT USIGNED
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;







