/*
 * for users: tenv1, tenv2, tenv3
 * 
 */
INSERT INTO user_roles VALUES(1, "admin");

INSERT INTO user_roles VALUES(1, "client");

INSERT INTO user_roles VALUES(2, "client");

INSERT INTO user_roles VALUES(3, "client");

INSERT INTO users VALUES(1, "iceberg", "cromlehg@gmail.com", "$2a$10$4vOGkYTCjOEKiFsYQkROkeWiG3JvfOfWiWNYjnkG6I8NNudcu8cRa", null, 1, 1, "Alexander", "Strakh", 1, 1463047451);

INSERT INTO users VALUES(2, "partizan", "lm333@mail.ru", "$2a$10$p6IBngt..Knt5450ixTHgOJ.ie0w6kMlsL3Kis9J/Xy8tibO9NR6m", null, 1, 1, "Michael", "Lozovikov", 1, 1463047451);

INSERT INTO users VALUES(3, "apetchaninov", "petchaninov@gmail.com", "$2a$10$H6XR3DQ8j2teuAOtvap4dusGTlRlB.sb/ESrvmZhtAc9muOO8Awmu", null, 1, 1, "Anton", "Petchaninov", 1, 1463047451);

INSERT INTO account_types VALUES(1, "Instagram", "1", null, null);

INSERT INTO accounts VALUES(1, 1, 1, '{"username":"cromlehg","password":"_______","deviceId":"8541dad7faf683c0","GUID":"2e17503a5-9321-4db2-8b6e-fb42ffa77168"}');

INSERT INTO robots VALUES(1, 1, "Complex robot", "1", null, null);

INSERT INTO proxies VALUES(1, "31.31.202.147", 1, 65233, "cromlehg", "_______");

INSERT INTO proxy_assigns VALUES(1, 1, 1, 1, null);

