#!/bin/bash
cd ds13.common
sbt clean compile eclipse publish
cd ../ds13.http
sbt clean compile eclipse publish
cd ../ds13.bots
sbt clean compile eclipse publish
cd ../ds13.bots.storable
sbt clean compile eclipse publish
cd ../ds13.insta
sbt clean compile eclipse publish
cd ../ds13.bots.storable.insta
sbt clean compile eclipse publish
cd ../ds13.rws
sbt clean compile eclipse
